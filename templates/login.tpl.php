  <div class="row">
    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 px-5 py-5">
      <h2>Login</h2>
      <p class="my-3 text-muted">To create new threads and to be able to add posts to the existing ones, you need a registered user account.
      If you don't already have an account, you can <a href="<?= BASE_URI ?>register/">register</a> for free.</p>
      <form method="POST" class="mt-3">  
        <?php 
        if(isset($loggedInSuccess) && $loggedInSuccess == 1): ?>
          <!-- Success error message -->
          <p class="alert alert-success">
            <strong>Congratulations !</strong> You have been logged in successfully.
          </p>
        <?php 
        // Has returned an error code
        elseif(isset($loggedInSuccess) && $loggedInSuccess > 100 && $loggedInSuccess < 107): ?>
          <!-- Login error message -->
          <p class="alert alert-danger">
            <strong>An error occured</strong>, please double check your username and your password.
          </p>
        <?php 
        endif; ?>
    
        <!-- Username -->
        <p>
          <input type="text" name="login[username]" class="form-control" placeholder="Username or e-mail adress" />
        </p>
        
        <!-- Password -->
        <p>
          <input type="password" name="login[password]" class="form-control" placeholder="Password" />
        </p>
        
        <p>
          <button class="btn btn-primary">Confirm</button>
          <a href="<?= BASE_URI ?>" class="btn btn-light">Abort</a>
        </p>
      </form>
    
    </div> <!-- /.col -->
  </div> <!-- /.row -->
</div> <!-- /.container -->