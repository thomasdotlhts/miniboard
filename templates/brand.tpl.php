<div class="row">  
  <div class="col-xl-6 col-lg-6 col-md-8 col-sm-12 col-12 pt-5 px-5">

    <h1><a href="<?= BASE_URI ?>">Miniboard</a></h1>
    <p class="text-secondary">A light-weight discussions program.</p>

  </div> <!-- /.col -->
</div> <!-- /.row -->


<div class="row">
  <div class="col-xl-6 col-lg-6 col-md-8 col-sm-12 col-12 pt-5 px-5">
    <?php 
    if(isset($_SESSION['username'])): ?>
      <p class="mb-5">Logged in as 
        <span class="bold"><?= htmlspecialchars($_SESSION['username']) ?></span>
      </p>
    <?php 
    endif; ?>
  </div> <!-- /.col -->
</div> <!-- /.row -->




<div class="row">
  <div class="col-xl-6 col-lg-6 col-md-8 col-sm-12 col-12 pb-4 px-5">
    
    <?php 
    if(is_file('install.php')): ?>
      <p class="alert alert-danger">
        <span class="bold">Remove the install.php file</span> to prevent any evil-mind to hijack your 
         Miniboard installation running the installer script again and this message will disapear.
      </p>
    <?php 
    endif; ?>
  </div> <!-- /.col -->
</div> <!-- /.row -->