<!-- Administration general configuration -->

<div class="row">  
  <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 px-0">
    
    <!-- Page title -->
    <div class="row mx-0">
      <div class="col-6 px-5">
        <h3 class="mb-5">Configuration</h3>
      </div> <!-- /.col -->
    </div> <!-- /.row -->
    
    <div class="row mx-0">
      
      <!-- 
            Project's metadatas 
            + + + + + + + + + + 
      -->
      <div class="col-12 px-5 mb-5 pb-5">
        <p class="bold">Project's metadatas</p>
        
        <form method="POST">
          <p>
            <input type="text" name="metadatas[title]" class="form-control" value="<?= !empty($title) ? htmlspecialchars($title):''; ?>"
                   placeholder="Community's project title" />
          </p>
          <p>
            <button class="btn btn-dark">Confirm</button>
          </p>
        </form>
      </div> <!-- /.col -->
    </div> <!-- /.row -->
    
    
    <div class="row mx-0">
      
      <!-- 
           Users special rights attribution
           + + + + + + + + + + + + + + + + + 
      -->
      <div class="col-12 px-5" id="rights">
        <p class="bold">Rights</p>
      
        <!-- Permissions revocation form -->
        <?php 
        if(isset($_GET['revoke']) && isset($_GET['right']) && isset($_GET['user_id'])): ?>
          <form method="POST" action="<?= BASE_URI ?>admin/config/#rights">
            <input type="hidden" name="revoke[right]" value="<?= htmlspecialchars($_GET['right']) ?>" />
            <input type="hidden" name="revoke[user_id]" value="<?= intval($_GET['user_id']) ?>" /> 
            <div class="alert alert-danger my-5">
              <p>
                <span class="bold">You are about to revoke the <?= $_GET['right'] == 'adm' ? 'administrators':'moderators' ?> rights</span> 
                to the selected user <span class="bold"><?= htmlspecialchars($usernamesIdsCouples[intval($_GET['user_id'])]['username']) ?></span>, are you sure 
                you want to perform this action ?<br />
                <button class="btn btn-danger btn-sm">Confirm</button>
                <a href="<?= BASE_URI ?>admin/config/" class="btn btn-light btn-sm">Abort</a>
              </p>
            </div> <!-- /.alert -->
          </form>
        <?php 
        endif; ?>
      </div> <!-- /.col -->
    </div> <!-- /.row -->

    
    <div class="row mx-0">    
      <div class="col-12 px-5"> 
       
         <div class="row mx-0 pb-5 mb-5 border-bottom">
           <div class="col-6 pl-0 pr-3">
             <p class="bold text-muted">Administrators</p>

             <?php 
             foreach($administratorsList as $id): ?>
                <a href="<?= BASE_URI ?>admin/config/revoke/adm/<?= $id ?>/#rights" class="badge badge-danger">
                  <?= htmlspecialchars($usernamesIdsCouples[$id]['username']) ?>
                  <span class="ml-2">&times;</span>
                </a>

             <?php 
             endforeach; ?> 

           </div> <!-- /.col -->
           
           <div class="col-6 pl-3 pr-0">
             <p class="bold text-muted">Moderators</p>
             <?php 
             foreach($moderatorsList as $id): ?>
                   <a href="<?= BASE_URI ?>admin/config/revoke/mod/<?= $id ?>/#rights" class="badge badge-primary">
                   <?= htmlspecialchars($usernamesIdsCouples[$id]['username']) ?>
                   <span class="ml-2">&times;</span>
                 </a>

             <?php 
             endforeach ?>

           </div> <!-- /.col -->
         </div> <!-- /.row -->
       
       
         <form method="POST">
            <div class="row mx-0 mb-5 border-bottom pb-5">
              <div class="col-6 pl-0 pr-3">
                <p>
                  Select an user :
                  <select name="rights[user_id]" class="form-control form-control-sm">
                    <?php 
                    foreach($usersList as $user): ?>
                      <option value="<?= intval($user['id']) ?>"><?= htmlspecialchars($user['username']) ?></option>
                    <?php 
                    endforeach; ?>
                  </select>
                </p>
                <p>
                  <button class="btn btn-sm btn-dark">Confirm</button>
                </p>
               </div> <!-- /.col -->
            
               <div class="col-6 pl-3 pr-0">
                 <p>
                   Select a right : 
                   <select name="rights[right]" class="form-control form-control-sm">
                     <option value="0">Moderator</option>
                     <option value="1">Administrator</option>                 
                   </select>                     
                 </p>
               </div> <!-- /.col -->
             </div> <!-- /.row -->
          </form>

       </div> <!-- /.col -->
     </div> <!-- /.row -->
     
     
    <!-- 
          Theme picker 
          + + + + + + +  
    -->
          

    <div class="row mx-0">    
      <div class="col-12 px-5"> 
        
        <p class="bold">Theme picker</p>
        
        <?php 
        // Apply theme success message 
        if(isset($themeUpdatedSuccess)): ?>
          <p class="alert alert-success my-3" id="message">
            <span class="bold">Congratulations !</span> The selected theme has been applied.
          </p>
        <?php 
        endif;
        ?>
        
        <form method="POST" action="<?= BASE_URI ?>admin/config/#message">
          <p>
            <select name="theme[custom]" class="form-control">
              <?php 
              foreach($themesList as $theme): ?>
                <option value="">Default</option>
                <option value="<?= htmlspecialchars($theme) ?>"><?= htmlspecialchars($theme) ?></option>
              <?php 
              endforeach; ?>
            </select>
          </p>
          <p>
            <button type="submit" class="btn btn-sm btn-dark">Confirm</button>
          </p>
        </form>
        
      </div> <!-- /.col -->
    </div> <!-- /.row -->
        
  </div> <!-- /.col -->
</div> <!-- /.row -->

<div class="my-5 py-5"></div>