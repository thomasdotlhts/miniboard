<div class="container-fluid">
  
  <div class="row">
    <div class="col-xl-6 col-lg-6 col-md-8 col-sm-12 col-12 pb-5 px-5">
      
      <form method="POST">
    
        <div class="row">
          <div class="col-6 px-0">
            <p class="bold"><?= htmlspecialchars($selectedProfile['username']) ?></p>
            <p class="text-small">Date registered : <?= date('Y-m-d H:i', $selectedProfile['timestamp']) ?></p>
            <p class="profile-image-container">
              <?php 
              if($selectedProfile['picture'] != null): ?>
                <img src="<?= BASE_URI . strip_tags(addslashes($selectedProfile['picture'])) ?>" class="border shadow-sm img-fluid" style="max-width:150px;" />
              <?php 
              endif ?>
            </p>
            </p>
          </div>
          <?php 
          if($isModerator || $isSelfProfile): ?>
          
            <!-- Update profile image -->
          
            <div class="col-6 px-0">
              <p class="bold">Picture</p>
              <p class="images-picker-group">
                <input type="file" name="images-picker" multiple="multiple" />
              </p>
              
              <!-- loader gif -->
              <p class="loader-gif d-none">
                <img src="<?= BASE_URI ?>static/img/loader.gif" alt="loader gif" />
              </p>
              
              <div class="pending" class="d-none mt-3 pb-2"></div>  <!-- thumbnails area -->
              <div class="links" class="my-3"></div> <!-- links zone -->
              
              <p>
                <button type="button" class="btn btn-sm btn-primary upload-btn">change</button>
              </p>
              
            </div> <!-- /.col --> 
          <?php 
          endif; ?>
        </div> <!-- /.row -->

        
        <!-- Your details -->

        <div class="row">
          <div class="col-12 px-0">
            <p class="bold mt-5">Details</p>
          </div> <!-- /.col -->
        </div> <!-- /.row -->
        
        <div class="row">
          <div class="col-12 px-0">
            
             <?php 
             if($isSelfProfile || $isModerator): ?>
               <form method="POST">
                  <p>
                    Location : 
                    <input type="text" name="profile[location]" class="form-control" placeholder="Your location" value="<?= !empty($selectedProfile['location']) ? htmlspecialchars($selectedProfile['location']):'' ?>" />
                  </p>
                  <p>
                    Biography :
                    <textarea class="form-control" name="profile[location]" rows="5" placeholder="Your biography"><?= 
                      !empty($selectedProfile['biography']) ? htmlspecialchars($selectedProfile['biography']):'' 
                    ?></textarea>
                  </p>
                  <p>
                    <button type="submit" class="btn btn-sm btn-primary">Update</button>
                  </p>
               </form>
             <?php 
             else: ?>
                <p>
                  <span>Location</span> :
                  <?= !empty($selectedProfile['location']) ? htmlspecialchars($selectedProfile['location']):' - '; ?>
                </p>
                <p>
                  <span>Biography</span> :
                  <?= !empty($selectedProfile['biography']) ? htmlspecialchars($selectedProfile['biography']):' - '; ?>
                </p>
             <?php 
             endif; ?> 
          
             
          
          </div> <!-- /.col -->
        </div> <!-- /.row -->  
      </form>
      
    </div> <!--/.col -->
  </div> <!-- /.row -->
</div> <!-- /.container-fluid -->

<link rel="stylesheet" href="<?= BASE_URI . 'static/css/style.css' ?>" />
<script src="<?= BASE_URI ?>static/js/input.js"></script>

<?php
if(($isLoggedIn && $selectedProfile['id'] == $_SESSION['id']) || $isAdministrator):
  echo 
    '<script>' . PHP_EOL
        . 'document.addEventListener(\'DOMContentLoaded\', function() {' . PHP_EOL
          . "\t" . 'let args = {'                                        . PHP_EOL
            . "\t\t" . 'inputSelector:\'input[type="file"]\','           . PHP_EOL
            . "\t\t" . 'thumbsContainerSelector: \'.pending\','          . PHP_EOL
            . "\t\t" . 'triggerSelector: \'.upload-btn\','               . PHP_EOL
            . "\t\t" . 'imageContainer: \'.profile-image-container\','   . PHP_EOL 
            . "\t\t" . 'url: \'' . BASE_URI . 'upload.php\','            . PHP_EOL
            . "\t\t" . 'customDirectory: \'static/uploads/avatars/\','   . PHP_EOL
            . "\t\t" . 'type: \'avatar\','                               . PHP_EOL
            . "\t\t" . 'imageURIPrefix: \'' . BASE_URI . '\','           . PHP_EOL
            . "\t\t" . 'maxNbOfFiles: 1'                                 . PHP_EOL
          . "\t" . '}' . PHP_EOL . PHP_EOL
      . "\t" . 'InputFiles.init(args);'                                  . PHP_EOL 
     . '})'                                                              . PHP_EOL
  . '</script>';
endif;
?>

<?php // EOF ?>