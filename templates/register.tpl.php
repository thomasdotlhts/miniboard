<div class="row">
  <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 px-5 py-5">
    <h2>Register</h2>
    <p class="my-3 text-muted">If you do not <a href="<?= BASE_URI ?>login">already have an account</a>, then fill the below form and create a free one ! Then you'll be able to create new threads and to add 
     posts to the existing ones.</p>
     
    <form method="POST">
      <?php 
      if(isset($userRegisteredSuccess)):
        // Errors and confirmations messages 
        switch($userRegisteredSuccess):
           case 1: ?>
             <!-- Registered successfully -->
             <p class="alert alert-success mb-4">
               <span class="bold">Congratulations</span> ! Your account has been created successfully.
             </p>
           <?php
           break;

           case miniboard\Models\Users::USERNAME_FORMAT_ERROR: ?>
             <!-- Username format invalid alert -->
             <p class="alert alert-danger mb-4">
               <span class="bold">The format of the username is invalid</span>, please specify an username that 
                  only contains alphanums from 3 to 15 characters length.
             </p>
           <?php
           break;

           case miniboard\Models\Users::USERNAME_UNAVALIABLE: ?>
             <!-- Username already in use -->
             <p class="alert alert-danger mb-4">
               <span class="bold">The given username is already in use</span>, please choose another one.
             </p>
           <?php
           break;

           case miniboard\Models\Users::EMAIL_FORMAT_ERROR: ?>
             <!-- E-mail format error alert -->
             <p class="alert alert-danger mb-4">
               <span class="bold">Email adress format error.</span>
             </p>
           <?php
           break;

          case miniboard\Models\Users::PASSWORD_FORMAT_ERROR: ?>
            <!-- Password format invalid alert -->
            <p class="alert alert-danger mb-4">
              <span class="bold">
                Please choose a stronger password.
              </span>
            </p>
          <?php
          break;

          case miniboard\Models\Users::PASSWORDS_MISMATCH: ?>
            <!-- Given passwords missmatch -->
            <p class="alert alert-danger mb-4">
              <span class="bold">
                <span class="bold">
                  The password and the confirmation missmatch.
                </span>
              </span>
            </p>
          <?php
          break;
       endswitch; 
     endif;?>
     
     
     <!--                       THE REGISTER FORM                            -->

     <!--                            USERNAME                                -->
     <p>
       <label class="bold" for="username">Username</label> :
       <input type="text" name="register[username]" class="form-control no-radius" id="username"
              value="<?= isset($_POST['register']['username']) ? htmlspecialchars($_POST['register']['username']):''; // autocompletion when needed ?>" placeholder="Choose an username" />
     </p>


     <!--                         EMAIL ADDRESS                              -->
     <p>
       <label for="email" class="bold">Email address </label> :
       <input type="text" name="register[email]" id="email"
              class="form-control" value="<?= isset($_POST['register']['email']) ? htmlspecialchars($_POST['register']['email']):''; // autocompletion when needed ?>"
              placeholder="Give a valid email address" />
     </p>


     <!--                            PASSWORD                                -->

     <p>
      <label for="password" class="bold">Password </label> :
      <input type="password" name="register[password]" class="form-control" id="password"
             value="<?= isset($_POST['register']['password']) ? htmlspecialchars($_POST['register']['password']):''; ?>"
             placeholder="Give a strong password" />
     </p>


     <!--                       PASSWORD CONFIRMATION                        -->
     <p>
       <label for="passwordConfirmation" class="bold">Password confirmation :</label>
       <input type="password" class="form-control" name="register[passwordConfirmation]" id="passwordConfirmation"
              placeholder="Type your password again" />
     </p>

     <!--                         Confirmation btn                           -->
     <p>
       <button class="btn btn-primary">Register</button>
       <a href="<?= BASE_URI ?>" class="btn btn-light">Back</a>
     </p> 
    </form>
  </div> <!-- /.col -->
</div> <!-- /.row -->