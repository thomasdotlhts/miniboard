  <div class="row">
    <div class="col-xl-6 col-lg-6 col-md-8 col-sm-12 col-12 py-5 px-5">
      <?php 
      if($isAdministrator): ?>
        <a href="<?= BASE_URI ?>admin/config">Dashboard</a>
      <?php 
      endif; ?>
    </div> <!-- /.col -->
  </div> <!-- /.row-->
  
</div> <!-- /.container-fluid -->

<!-- highlight.js -->
<script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.15.8/highlight.min.js"></script>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.15.9/styles/tomorrow-night-blue.min.css">
<script>
document.addEventListener('DOMContentLoaded', (event) => {
  document.querySelectorAll('pre code').forEach((block) => {
    hljs.highlightBlock(block);
  });
});
</script>