<div class="row">  
  <div class="col-xl-6 col-lg-6 col-md-8 col-sm-12 col-12 pb-5 px-5">

    <?php
    if(!empty($_GET['moderate']) && ($isModerator || $isAdministrator)): ?>
      <!-- This form is used to send moderation actions -->
      <form method="POST">
    <?php 
    endif; ?>

    <div class="row mx-0 mb-3">
       <div class="col-8 px-0 py-0 bold text-small">
         Thread
       </div> <!-- /.col -->
       <div class="col-4 px-0 py-0 bold text-small">
         Details
       </div> <!-- /.col -->
    </div> <!-- /.row -->


    <!-- 
          Pinned threads list
          + + + + + + + + + + 
    -->

    <?php 
    if(!empty($pinnedThreadsList)): ?>
      
      <?php
      foreach($pinnedThreadsList as $thread): ?>
      
      
        <!-- A line per thread -->
        <div class="row mx-0">
          <div class="col-8 px-0 pt-3">
            <?php 
            if(isset($_GET['moderate']) && ($isModerator || $isAdministrator)): ?>
               <!-- Moderate checkbox -->
               <input type="checkbox" class="mr-3" name="moderate[<?= intval($thread['id']) ?>]" />    
            <?php 
            endif; ?>
                               
            <!-- Thread's title -->
               
            <!-- Title text is muted if thread is locked -->
            <a href="<?= BASE_URI ?>thread/<?= intval($thread['id']) ?>/" class="bold <?= ($thread['locked'] ? 'text-muted':''); ?>">
              <?= htmlspecialchars($thread['title']); ?>   
            </a>
            
            <!-- Pinned reminder badge  -->
            <span class="badge badge-light ml-2">pinned</span>
            
           </div> <!-- /.col -->   
           
           <div class="col-4 px-0 pt-3">
               <p class="mb-0 text-small">
                 By <span class="bold"><?= htmlspecialchars($thread['username']); ?></span>
               </p>
               <p class="text-small">
                 <span class="text-muted">Last post : <?= date('Y - M - d h:i', $thread['last_reply']) ?></span>
               </p>
           </div> <!-- /.col -->
        </div> <!-- /.row -->
      <?php 
      endforeach; ?>
      
    <?php 
    endif; ?>




    <!-- 
         Classics threads list 
         + + + + + + + + + + + 
    -->
      
    <?php
    if(!empty($threadsList)): 
      
      foreach($threadsList as $thread): ?> 
        <!-- A line per thread -->
        <div class="row mx-0">
          <div class="col-8 px-0 pt-3">
            <?php 
            if(isset($_GET['moderate']) && ($isModerator || $isAdministrator)): ?>
               <!-- Moderate checkbox -->
               <input type="checkbox" class="mr-3" name="moderate[<?= intval($thread['id']) ?>]" />    
            <?php 
            endif; ?>
                   
            <!-- Thread's title -->
                   
            <!-- Title text is muted if thread is locked -->
            <a href="<?= BASE_URI ?>thread/<?= intval($thread['id']) ?>/" class="<?= ($thread['locked'] ? 'text-muted line-through':''); ?>">
              <?= htmlspecialchars($thread['title']); ?>   
            </a>
            
           </div> <!-- /.col -->   
           
           <div class="col-4 px-0 pt-3">
               <p class="mb-0 text-small">
                 By <span class="bold"><?= htmlspecialchars($thread['username']); ?></span>
               </p>
               <p class="text-small">
                 <span class="text-muted">Last post : <?= date('Y - M - d h:i', $thread['last_reply']) ?></span>
               </p>
           </div> <!-- /.col -->
        </div> <!-- /.row -->
       <?php 
       endforeach; 
       
    elseif(!$threadsCount): ?>
    
      <!-- As their is no thread to display -->
      
      <p class="alert alert-info">
        <span class="bold">Welcome to your new board, unfortunately</span> their is nothing to display. You can 
         create a thread at any moment using the above form.
      </p> 
       
   <?php 
   endif; ?>
    
        
   <!-- Pagination component -->
   <div class="row mx-0">
     <div class="col-12 px-0 pt-3">
       <?php 
       if(!empty($paginationComponentDOM))
          echo $paginationComponentDOM; ?>
     </div> <!-- /col -->
   </div>
   
   
   <?php 
   if($isModerator || ($isAdministrator || $isModerator)): ?>
      <div class="row mx-0">
        <div class="col-12 px-0 text-right">
          <?php
          if(!isset($_GET['moderate'])): ?>
            <!-- Moderate mode toggle link -->
            <a href="<?= BASE_URI . (isset($_GET['page']) ? 'p/' . intval($_GET['page']) . '/':'') . 'mod/' ?>">
              Moderate
            </a>
          <?php 
          else: ?>
            <p>
               <!-- Moderate menu -->
               <span class="text-secondary">Moderate :</span> 
               <button name="moderate[pin]"   class="btn btn-sm btn-light">pin</button>
               <button name="moderate[unpin]"   class="btn btn-sm btn-light">unpin</button>
               <button name="moderate[lock]"   class="btn btn-sm btn-light">lock</button>
               <button name="moderate[unlock]" class="btn btn-sm btn-light">unlock</button>
               <button name="moderate[delete]" class="btn btn-sm btn-light">delete</button>
               <a href="<?= BASE_URI . (isset($_GET['page']) ? 'p/' . intval($_GET['page']) . '/':'') ?>" class="btn btn-sm btn-light">abort</a>
            </p>
          <?php   
          endif; ?>
        </div> <!-- /col -->
      </div> <!-- /row -->
   <?php 
   endif; ?> 
   
   <?php 
   if(isset($_GET['moderate']) && ($isModerator || $isAdministrator)): ?>
     </form>
   <?php 
   endif; 

   if($isLoggedIn && empty($_GET['moderate'])): ?>
   
     <!-- Write a thread form -->
     <form method="POST" class="mt-5 px-3 py-3 border">
         <h6 class="mb-3 bold">Write a thread</h6>
         <p>
           <input type="text" name="thread[title]" placeholder="Your post title" class="form-control" />
         </p>
         <p>
           <textarea name="thread[content]" rows="5" class="form-control" placeholder="Write your post's content here"></textarea>
           <button class="btn btn-primary mt-3">post</button>
         </p>
      </form>
     <?php 
     elseif(empty($_GET['moderate'])): ?>
       <p class="d-block mt-5 pt-5 border-top">
         Please <a href="<?= BASE_URI ?>login">login</a> or <a href="<?= BASE_URI ?>register">create an account</a> to write a new post 
         and answer the existing ones.
       </p>
     <?php 
     endif; ?> 
    
  </div> <!-- /.col -->
</div> <!-- /.row -->