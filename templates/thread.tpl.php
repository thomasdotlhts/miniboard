<div class="row">
  <div class="col-xl-6 col-lg-6 col-md-8 col-sm-12 col-12 pb-5 px-5">




    <!-- 
          Thread metadatas display
          + + + + + + + + + + + + +
                                    -->

    <div class="row mx-0">
      <div class="col-10 px-0">
        <h2 class="mb-4 <?= $postsList[0]['locked'] ? 'line-through':''; ?>">
          <?= htmlspecialchars($postsList[0]['title']) ?>
        </h2>
      </div> <!-- /.col -->


      <?php 
      if(!isset($_GET['edit']) && $isLoggedIn && (intval($postsList[0]['owner']) == $_SESSION['id']) ): ?>
        <div class="col-2 px-0 text-right">
          <a class="btn btn-light text-secondary" href="<?= BASE_URI ?>thread/<?= intval($postsList[0]['thread_id']) ?>/e/">
            edit
          </a> 
        </div> <!-- /.col -->
      <?php 
      endif; ?> 
    </div> <!-- /.row -->
    
    <?php 
    if(isset($_GET['moderate']) && ($isModerator || $isAdministrator)): ?>
      <form method="POST">
    <?php
    endif; ?>
    
    <?php 
    if(!isset($_GET['edit'])): ?>
    
      <div class="row mx-0">
         <div class="col-3 px-0 pb-3 px-3 bold">Username</div>
         <div class="col-9 px-0 pb-3 px-3 bold">Message</div> <!-- /.col -->
      </div> <!-- /.row -->

      <?php   
      foreach($postsList as $post): ?>
      
         <!-- A line per post -->
      
         <div class="row mx-0" id="<?= intval($post['id']) ?>">
           <div class="col-3 px-0 pt-3 pb-3 bg-light px-3 mb-3">
              <a href="#" class="d-block mb-2">
               <!-- Message owner username -->
               <?= htmlspecialchars($post['username']); ?>
              </a>
              
              <p>
                <?php 
                if($post['picture'] != null): ?>
                  <img src="<?= BASE_URI . strip_tags(addslashes($post['picture'])) ?>" class="border shadow-sm img-fluid" />
                <?php 
                endif ?>
              </p>
           
            </div> <!-- /.col -->
            <div class="col-9 px-3 pt-3 pb-3">
              
              <p class="text-small text-muted">
                <!-- Posted date and hour -->
                <?= date('Y.m.d h:i', $post['timestamp']); ?>
              </p>  
              
              <p style="color: #444;">
                <!-- Message content -->
                <?= $post['content']; ?>
              </p>
          
              <!-- Buttons area -->
              <div class="mt-5 pt-3 border-top">
                
                <!-- Deletion confirmation message --> 
                <?php 
                if(isset($_GET['delete']) && isset($_GET['post']) && ($_GET['post'] == $post['id'])): ?>
                
                  <div class="alert alert-danger">
                    <span class="bold">You are about to delete a post</span>, this action is irrecuperable and it won't be recoverable,
                    are you sure you want to do this ?
                    <!-- deletion confirmation form -->
                    <form method="POST" action="<?= BASE_URI ?>thread/<?= intval($post['thread_id']) ?>/">
                      <input type="hidden" name="delete[post][id]" value="<?= intval($_GET['post']) ?>" />
                      <button class="btn btn-sm btn-danger" type="submit">confirm</button>
                      <a href="<?= BASE_URI ?>thread/<?= intval($_GET['id']) ?>/<?= (isset($_GET['page']) ? 'p/' . intval($_GET['page']) .'/':'') ?>" class="btn btn-sm btn-abort">Abort</a>
                    </form>
                  </div> <!-- /.div -->
                
                <?php 
                endif; ?>
                
                <a class="mr-3 text-small" href="<?= BASE_URI ?>thread/<?= intval($post['thread_id']) ?>/#<?= intval($post['id']) ?>">
                  #<?= intval($post['id']) ?>
                </a>
                <?php 
                if((isset($_SESSION['id']) && ($post['owner'] == $_SESSION['id'])) || ($isModerator || $isAdministrator) || true): ?>
                  <a href="<?= BASE_URI ?>thread/<?= intval($post['thread_id']) ?>/<?= intval($post['id']) ?>/#post" class="mr-3">Edit</a>
                  <a href="<?= BASE_URI ?>thread/<?= intval($post['thread_id']) ?>/del/<?= intval($post['id']) ?>/#<?= intval($post['id']) ?>">Delete</a>
                <?php 
                endif; ?>
              </div>
            
              <?php 
              if(isset($_GET['moderate']) && ($isModerator || $isAdministrator)): ?>
                <div class="row">
                  <div class="col-12 text-center bg-light mx-2 my-2 py-2">
                    <input type="checkbox" name="moderate[<?= intval($post['id']) ?>]" />
                  </div> <!-- /.col -->
                </div> <!-- /.row -->
              
              <?php 
              endif; ?>
            </div> <!-- /.col -->
          </div> <!-- /.row -->
       <?php 
       endforeach; ?>
       
     
       
       <!-- Pagination component -->
       <div class="row mx-0">
         <div class="col-12 px-0">
           <?php 
           if(!empty($paginationComponentDOM))
             echo $paginationComponentDOM; ?>
         </div> <!-- /col -->
       </div>




       <!--
              Moderate menu 
              + + + + + + +
                             -->

       <?php 
       if(($isModerator || $isAdministrator)): ?>
          <div class="row mx-0">
            <div class="col-12 px-0 text-right">
              <?php
              if(!isset($_GET['moderate'])): ?>
                <!-- Moderate mode toggle link -->
                <a href="<?= BASE_URI . 'thread/' . intval($_GET['id']) . '/' . (isset($_GET['page']) ? 'p/' . intval($_GET['page']) . '/':'') . 'mod/' ?>">
                  Moderate
                </a>
              <?php 
              else: ?>
                <p>
                   <!-- Moderate menu -->
                   <span class="text-secondary">Moderate :</span> 
                   <button name="moderate[delete]" class="btn btn-sm btn-light">delete</button>
                   <a href="<?= BASE_URI . 'thread/' . intval($_GET['id']) . '/' . (isset($_GET['page']) ? 'p/' . intval($_GET['page']) . '/':'') ?>" class="btn btn-sm btn-light">abort</a>
                </p>
              <?php   
              endif; ?>
            </div> <!-- /col -->
          </div> <!-- /row -->
       <?php 
       endif; ?> 
       
       <?php 
       if(isset($_GET['moderate']) && ($isModerator || $isAdministrator)): ?>
          </form> <!-- end moderate form -->
       <?php 
       endif; ?>




       <?php
       if($isLoggedIn && !$postsList[0]['locked']): ?>


         <!-- 
                Add a message to the thread form 
                + + + + + + + + + + + + + + + + +       
                                                   -->
         
         <form method="POST" class="mt-5 px-3 py-3 border" id="post">
            <h6 class="mb-3 bold">Write a post</h6>
            <?php 
            if(isset($postUpdatedSuccess) && $postUpdatedSuccess): ?>
              <p class="alert alert-success">
                <strong>Congratulations !</strong> The post has been saved.
              </p>
            <?php 
            endif; 
         
            if(!empty($selectedPost)): ?>
              <input name="post[id]" type="hidden" value="<?= intval($selectedPost['id']) ?>" />
            <?php 
            endif; ?>
            <p>
              <textarea name="post[content]" class="form-control <?= !empty($selectedPost) ? 'border-warning':'' ?>" rows="5"><?=
               !empty($selectedPost) ? htmlspecialchars($selectedPost['content']):'';
              ?></textarea>
            </p>
            <p>
              <button class="btn btn-primary">post</button>
              <?php 
              if(!empty($selectedPost)): ?>
                <a class="btn btn-light" href="<?= BASE_URI ?>thread/<?= intval($_GET['id']) ?>/">abort</a>
              <?php 
              endif; ?>
            </p>
           </form>
        <?php 
        elseif($postsList[0]['locked']): ?>
          <p class="d-block mt-5 pt-5 border-top">This thread has been locked by a moderator so you can't write new posts.</p>
           
        <?php 
        else: ?>
          <p class="d-block mt-5 pt-5 border-top">
            Please <a href="<?= BASE_URI ?>login">login</a> or <a href="<?= BASE_URI ?>register">create an account</a> to write a new post 
            and answer the existing ones.
          </p>      
        <?php 
        endif; 
      
      else: ?>



  
      <!-- 
           Thread edition single-page mode 
           + + + + + + + + + + + + + + + +
                                            -->
      
        <div class="row mx-0">
          <div class="col-12 bg-light px-3 pt-3">
            <form method="POST">
              <?php 
              if(isset($threadUpdatedSuccess) && $threadUpdatedSuccess): ?>
                <p class="alert alert-success">
                  <span class="bold">Congratulations !</span> The thread has been updated.
                </p>
              <?php 
              endif; 
               
              if(!empty($selectedThread)): ?>
                <input type="hidden" name="thread[id]" value="<?= intval($selectedThread['id']) ?>" />
                <input type="hidden" name="thread[first_post_id]" value="<?= intval($selectedThread['first_post_id']) ?>" />
              <?php 
              endif; ?>
              
              <h6 class="mb-3">Edit a thread</h6>
              <p>
                <input type="text" name="thread[title]" placeholder="Your post title" class="form-control <?= !empty($selectedThread) ? 'border-warning':''; ?>" value="<?= !empty($selectedThread) ? htmlspecialchars($selectedThread['title']):'' ?>" />
              </p>
              <p>
                <textarea name="thread[content]" rows="5" class="form-control <?= !empty($selectedThread) ? 'border-warning':''; ?>" placeholder="Write your post's content here"><?= 
                  !empty($selectedThread) ? $selectedThread['content']:''; 
                ?></textarea>
                
                <button class="btn btn-primary mt-3 mr-2">post</button>
                <a href="<?= BASE_URI ?>thread/<?= intval($_GET['id']) ?>/" class="btn btn-light mt-3">back</a>
              </p>
            </form>
          </div> <!-- /.col -->
        </div> <!-- /.row -->
              
      <?php 
      endif; ?>
  </div> <!-- /.col -->
</div> <!-- /.row -->
<?php // EOF ?>