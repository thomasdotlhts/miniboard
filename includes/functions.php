<?php
/* Instanciates PDO with the `config.php` given database configuration then
 * return the newly instanciated PDO object */
 
function __db($host, $username, $password, $db, $silent = true) {
  if(!class_exists('PDO')) { 
    echo 'PDO unreacheable.'; 
    die(); 
  } 

  try {
    $PDO = new PDO('mysql:host=' . $host . ';dbname=' . $db . ';charset=utf8mb4', $username, $password,
                   [PDO::ATTR_ERRMODE  => PDO::ERRMODE_EXCEPTION, PDO::ATTR_EMULATE_PREPARES  =>  0]);

    if(!$PDO): __db_error(); endif;
    return $PDO;
  }

  catch(PDOException $e){
    if(!$silent) {
      echo $e . '<br>';
    }
  }
}




// Write a " you are not allowed message " and exit. 
function _exit() {
  echo '<p>You are not allowed to do this.<br /><a href="' . BASE_URI . '">Go back</a></p>';
  exit();

}

// EOF