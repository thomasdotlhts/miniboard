<?php
namespace miniboard\includes;
use \Exception;




/** The Miniboard Models class, that is a frame to write Models.  
  *
  * @author : Thomas LHTS <thomas.lhts[at]protonmail.ch>
  * @date: 2019.03.25
  * @license : https://opensource.org/licenses/MIT
  * @version: 0.1a */
  
abstract class Models {
    
    
  /** This array contains values to be used for the next SQL query.
    * It has to be resetted between each query. 
    * @var array $values */ 
  protected $values = [];


 /** This array contains values meant to be used as configuration values to 
   * influence the class' behaviour. */
 protected $props = []; 
  

  
  /** Is used for injection of dependencies.
    * @var array $injected */
  protected $injected = [];
  



  /** Related to pagination. Specify the current page to craft the LIMIT query
    * while fetching datas. 
    * @var array $currentPage */
  protected $currentPage = null;


  /** Related to pagination. Specify the limit of items per pages to craft the 
    * LIMIT query while fetching datas. 
    * @var array $limitPerPage */
  protected $limitPerPage = 0;
  
  
  /** Number of total elements to be paginated. Used to compute the values 
    * used as we craft the LIMIT query. 
    * @var array $paginationNbrOcc */
  protected $paginationNbrOcc = 0;




  /** Tables prefix - cf. Models::setTablesPrefix().
    * @var null|integer $prefixed */
  protected $prefixed = null;

  // + + + + + + + + + + +




  /** As the Miniboard project's installer offers to specify a 
    * table prefix at the database configuration step, then here is an helper 
    * method to append the prefix to the tables names. 
    * @return object The instance itself. */
     
  public function &setTablesPrefix() : object {
    
    if(is_array($this->tables) && $this->prefixed): // as several tables names are specified as an array of strings
      
      foreach($this->tables as $k => $t)
        $this->tables[$k] = $this->prefixed . $t;
    endif;
    return $this;
  }

  // + + + + + + + + + +




  /** Somehow a general-purpose mutator. 
    * @param string $key The key to associate the data with in the ArticlesModel::$values array. 
    * @param mixed $value The value to be associated to the specified key. 
    * @return self The object itself. */
    
  public function &set($key, $value) {
    $this->values[$key] = $value;
    return $this;
  }
  
  // + + + + + + + + + +   


  
  
  /** Somehow a general-purpose mutator for configuration props. 
    * @param string $key The key to associate the data with in the ArticlesModel::$values array. 
    * @param mixed $value The value to be associated to the specified key. 
    * @return self The object itself. */
    
  public function &setProp($key, $value) {
    $this->props[$key] = $value;
    return $this;
  }
  
  // + + + + + + + + + +   
  

  
  
  
  /** Store an injection of dependency.
    *
    * @param string $key The key the given object has to be associated with in 
    *                    the Models::$injected array.
    *
    * @param object $value An instanciated object of any dependency.
    *                                                                         */
  
  public function &inject(string $key, object $value) : object {
    $this->injected[$key] = $value;
    return $this;
  }
  
  // + + + + + + + + + 
  
  
  
  
  // Related to pagination 
  
  
  /** Assign the current page by reference. It is not necessary that the 
    * specified variable actually exists and it won't display any error if 
    * it does not exist : in this case, the value will be NULL.
    *
    * @param $var A reference to a variable that actually contains the current 
    *             page value. It is basically an index of the $_GET superglobal 
    *             i.e $_GET['page'].      
    *                                    
    * @return object The instance itself. */
    
  public function setCurrentPage(&$var) : object {

    @$this->currentPage =& $var;
    return $this;
  }




  /** Get the current page. 
    * @return int The current page. */
  public function getCurrentPage() : int {
    return $this->currentPage;
  }




  /** Set the limit of items per page. Will be used to craft the LIMIT request 
    * of the SELECT queries that has to take care of pagination. 
    * @param int $limit The limit of items per page.
    * @return object The instance itself. */
    
  public function setLimitPerPage(int $limit) : void {
    $this->limitPerPage = $limit;
  }
  
  
  
  
  /** Return the number of pages after a SELECT query has been executed. 
    * @return int Number of pages. */
  
  public function getNumberOfPages() : int {
    return $this->nbOfPages;
  }
  
  // + + + + + 




  /** Generate the LIMIT clause to be appended to any SELECT query that 
    * has to take care of pagination. 
    * @param string $table The involved table name as string. 
    * @param string $whereClause (optional) an additional WHERE clause that  
    *               filters the results to be paginated. 
    */
  public function paginate(string $table, string $whereClause = '') : string {
    
    // @WARNING : We are all adults here take care about the $whereClause parameter & SQL injections.
    
    
    if(!$this->limitPerPage) return ''; // pagination is disabled if no limited per page has been specified
    $this->currentPage = !$this->currentPage ? 1:$this->currentPage; // default page is always 1
    
    
    /* The purpose of this function is to be called by the overrided fetch() 
       child function. It returns the generated extra LIMIT clause to be appended 
       to the query written into the child's fetch method. This way, we get pagination 
       encapsulated for all objects. */

    if($this->limitPerPage): // pagination is enabled 
    
      // Count the total number of items in the specified table 
      // @TODO : Give a way to specify additional WHERE filters here to match the possible child's fetch methods ones. 
      $whereClause = !empty($whereClause) ? ' WHERE ' . $whereClause:'';
      $nbrOcc =  @$this->injected['dbh']->query('SELECT COUNT(id) AS nbr_occ FROM ' . $table . $whereClause)
                                        ->fetch()['nbr_occ'];
                                                                        
      if(!is_int($nbrOcc)) throw new Exception(__METHOD__ . ' : can\t count occurences from database.');
      
      $this->paginationNbrOcc = $nbrOcc;
      $this->nbOfPages = ceil($nbrOcc / $this->limitPerPage);
      $this->currentPage = ($this->currentPage > $this->nbOfPages) ? 1:$this->currentPage;
      $this->firstElement = ($this->currentPage - 1) * $this->limitPerPage;
      
      // Returns the LIMIT clause to be appended to the query
      return sprintf(' LIMIT %d OFFSET %d', $this->limitPerPage, $this->firstElement);
    endif; 
    
    return '';
  }
  
  // + + + + + + + + + +
  
}
// EOF
