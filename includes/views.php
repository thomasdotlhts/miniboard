<?php
namespace miniboard\includes;
use \Exception;




/** The Miniboard Views class that is a frame to write Views. 
  *
  * @author : Thomas LHTS <thomas.lhts[at]protonmail.ch>
  * @date: 2019.03.25
  * @license : https://opensource.org/licenses/MIT
  * @version: 0.1a */
  
abstract class Views {


  /** A list a value to be accessible through the template's scope.
    * @var array $vars */
  protected $vars = [];


  /** Dependencies injection to Views array.
    * @var array $injected */
  protected $injected = [];
  
  
  
  
  // Related to templates files 

  
  /** The templates file directory's path. 
    * @var string $templatesDirectory */
  private static $templatesDirectory = 'templates/';


  /** A list of files to be loaded as templates.
    * @var array $files */
  protected $files = [];
  
  
  /** A possible specified theme to overwrite the current template 
    * @var null|string $theme */
  private static $theme = null;
  
  
  /** The list of existing themes after the Views::themes() methods has been 
    * called stored an as array of strings.
    * @var null|array */ 
  private static $themes = null;

  // + + + + + + + + + +   




  /** @desc Assign a value => key pair to be exported to the scope of the generated template.
   *  @param string $name The to be variable's name.
   *  @param mixed $value The to be variable's value.
   *  @return int */
   
  protected function &setVar($name, $value) : object {

    list($name, $value) = [trim((string) $name), $value];
    $this->vars[$name] = $value;
    return $this;
  }




  /** @desc Make a template file to be loaded. Final template is displayed as all loaded files stacked to others.
    * @param string $filename A file to be stacked as template's piece.
    * @return int */
    
  protected function &setFile($filename) : object {
    // @TODO : Maybe we'll ensure some check on files later but its not a big concern for now as we're all adults here.
    
    $this->files[] = (string) $filename;
    return $this;
  }


  

  /** @desc Mecanism used to provide dependencies injections to Views to make them accessibles in 
    *       the scope of the `onDisplay` method. 
    * @param mixed $key Name of the variable in the scope of the `onDisplay` method.
    * @param mixed $value Value assigned to the provided key. */
  
  public function &inject($key, $value) : object {
    $this->injected[$key] = $value;
    return $this;
  }

  // + + + + + + + + + + 




  /** As we'll need some logic before everything to get displayed, we allow a 
    * callback to be registered to be executed before templates file are loaded.
    * @return mixed */
    
  protected function onDisplay() {}




  /** @desc The display method.
   *  @return mixed May return callback return value. */
   
  public function display() {
    
    $callableReturned = $this->onDisplay(); // additionnal user defined logic hook
    extract($this->vars); // template variables are extracted to the scope of the templates files 
    
    // require all templates files in the order they were registered
    foreach($this->files as $file): 
      $fileFound = 0;

      // Determine wether a custom theme is in use for this file or if we have to load the default file. 
      
      if(!empty(self::$theme)):
        
        // Custom file 
        $customFile = self::$templatesDirectory . self::$theme . '/' . $file;
        if(is_file($customFile)): // require the custom template file 
          $fileFound = 1;
          require $customFile;
          continue;
        endif;
      endif;
        
      // Default file 
      $defaultFile = self::$templatesDirectory . $file;
      if(is_file($defaultFile)): // require the default template file 
        require $defaultFile;
        continue;
      endif;
      
      throw new Exception(sprintf('%s : error - `%s` template file not found on server.', __METHOD__, $file));
     endforeach;
       
     return $callableReturned;
  }
  
  // + + + + + + + + + + 
  



  /* Related to the themes picker 
  
     As the View models also renders the template ; it also provides some helpers 
     methods & properties to deal with themes. */
     
     
  /** Returns the list of themes as subdirectories of the templates/ folder. 
    * If a value is specified for the theme argument, this value will be used 
    * to specify a theme overwrite with a custom theme. */
    
  public static function themes(string $customTheme = null) : array {
          
    if(!empty(self::$themes)):
      return self::$themes;
      
    else: 
      // Build the list of themes
      $themes = [];
      foreach(glob(self::$templatesDirectory . '*', GLOB_ONLYDIR) as $theme)
        $themes[] = str_replace(self::$templatesDirectory, '', $theme);

      self::$themes = $themes;

    
      // Set the theme 
      if(!empty($customTheme) && in_array($customTheme, $themes))
          self::$theme = $customTheme;
          
      return $themes;
     endif;
  }
  
  // + + + + + + + + + + 




  // Related to components 
  
     
  /** Generate BS4 pagination component.
    *
    * @param int $currentPage The current page to be displayed as active. 
    * @param int $nbOfPages The total number of pages.
    * @param string $uriPrefix The prefix to be appended at the start of the href attribute of generated pages links. 
    *
    * @return string The DOM as string of the generated pagination component. */
  protected static function _component_pagination(int $currentPage, int $nbOfPages, string $uriPrefix = '') : string {
    
    $element = '<nav aria-label="Page navigation example">' . PHP_EOL;
    $element .=  "\t" . '<ul class="pagination pagination-sm">'           . PHP_EOL;
    
    if(($currentPage - 1))
      $element .= "\t\t" .   '<li class="page-item"><a class="page-link" href="' . $uriPrefix . ($currentPage - 1) . '/"><span aria-hidden="true">&laquo;</span></a></li>'                . PHP_EOL;
    
    for($i = 0; $i < $nbOfPages; $i++)
      $element .= "\t\t" .   '<li class="page-item ' . ( (($i + 1) == $currentPage) ? 'active':'') .  '"><a class="page-link" href="' . $uriPrefix . ($i + 1) . '/">' . ($i + 1) . '</a></li>' . PHP_EOL;
    
    if(($currentPage + 1) <= $nbOfPages)
      $element .=  "\t\t" .  '<li class="page-item"><a class="page-link" href="' . $uriPrefix . ($currentPage + 1) . '/" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>'         . PHP_EOL;
    
    $element .= "\t" . '</ul>' . PHP_EOL;
    $element .= '</nav>'       . PHP_EOL;
    
    return $element;
  }

}
// EOF
