<?php 
namespace miniboard\Views;

use miniboard\includes\Views, 
    miniboard\Models\ThreadsDbLayer, 
    miniboard\Models\PostsDbLayer; 




/** The Miniboard Home View displays the list of threads in the form of a list 
  * of links that lead to the threads themselves. 
  *
  * It also processes the moderation actions sent as POST data such as locking, 
  * unlocking & deleting existing threads.  
  *          
  * @author  :  Thomas LHTS <thomas.lhts[at]protonmail.ch>
  * @date    :  2019.07
  * @license :  https://opensource.org/licenses/MIT
  * @version :  0.1a                                                          */

class HomeView extends Views {
  
  public function onDisplay() : void {


    $Threads = new ThreadsDbLayer();
    $Threads->inject('dbh', $this->injected['dbh']);
    $Threads->cleanOprhans();
    
    $Posts = new PostsDbLayer();
    $Posts->inject('dbh', $this->injected['dbh']);

    $loggedIn = $this->injected['Users']->isLoggedIn();
    $isAdministrator = $this->injected['Config']->isAdministrator();  
    $isModerator = $this->injected['Config']->isModerator();


    // Status shorthands to template         
    $this->setVar('isLoggedIn', $loggedIn);
    $this->setVar('isAdministrator', $isAdministrator);
    $this->setVar('isModerator', $isModerator);

    // + + + + + + + + + + + 




    if(!empty($_POST)):
      
      if($this->injected['Users']->isLoggedIn()): // only logged-in users may add new threads
        
         // Create a new thread 
         if(!empty($_POST['thread']) && !empty($_POST['thread']['title']) && !empty($_POST['thread']['content'])):
      
            // Add new thread metadatas to the database 
            $Threads->set('owner', $_SESSION['id'])
                    ->set('title', $_POST['thread']['title'])
                    ->set('start', 0)
                    ->set('locked', 0)
                    ->set('pinned', 0)
                    ->save();
                    
            if($Threads->lastInsertId()): // we rely on the last inserted thread's id to add the first post to the database.

              $Posts->set('owner', $_SESSION['id'])
                    ->set('thread_id', $Threads->lastInsertId())
                    ->set('content', $_POST['thread']['content'])
                    ->set('timestamp', time())
                    ->save();
                    
              if($Posts->lastInsertId()): // we set this post's last inserted id as new thread's first post id                  

                // Set the first post of the thread
                $Threads->set('id', $Threads->lastInsertId())
                        ->set('start', $Posts->lastInsertId())
                        ->setStartId();

                
              else:

                /* If a thread has been inserted but the related first post insertion failed, this means the database 
                   has been populated with an orphan thread. A oprhan threads cleanup is required to preserve thread's 
                   list consistency. */
                $Threads->cleanOrhans();
                
              endif;     
            endif;
                          
         endif;
       endif;
    endif;
  
    // + + + + + 
    
    
    
  
    // Process moderator actions 
  
    if(isset($_POST['moderate'])):
  
      $foundIds = []; // several ids may have been sent on POST 
      $mode = null;
      foreach($_POST['moderate'] as $k => $v):
        
        // Process thread id involved by the moderate mode form submission 
        if(is_numeric($k)):
          $foundIds[] = (int) $k;
          
        else: 
          
          // Process verb
          switch($k):
            case 'lock':
            case 'unlock':
            case 'pin':
            case 'unpin':
            case 'delete':
              $mode = $k;
            break;
          endswitch;
        endif;
      endforeach;
      
      // Apply selected action to the involved thread
      switch($mode):
        
        // Lock or unlock specified thread(s)
        case 'lock':
        case 'unlock':
          $Threads->set('id', $foundIds);
          $Threads->lock(($mode == 'lock' ? 0:1)); // 0 is lock ; 1 is unlock
        break;
        
        // Pin or unpin the specified thread(s)
        case 'pin':
        case 'unpin':
          $Threads->set('id', $foundIds);
          $Threads->pin(($mode == 'pin' ? 0:1)); // 0 is pin ; 1 is unpin
        break;
        
        // Delete specified thread(s)
        case 'delete':
          $Threads->set('id', $foundIds);
          $Threads->delete();
        break;
        
      endswitch;
    endif; 
  
    // + + + + + + + + + + 
  
  
    // Get the threads list 
    
    $Threads->setCurrentPage($_GET['page']);
    $Threads->setLimitPerPage(10);
    
    $this->setVar('pinnedThreadsList', $Threads->pinned())
         ->setVar('threadsList',  $Threads->fetch())
         
         ->setVar('paginationComponentDOM', self::_component_pagination($Threads->getCurrentPage(), $Threads->getNumberOfPages(), sprintf('%sp/', BASE_URI)))
         
         // The thread counter has to take care of the total number of pinned & standard threads 
         ->setVar('threadsCount', count($this->vars['threadsList']) + count($this->vars['pinnedThreadsList']))
         
         ->setFile('header.tpl.php')
         ->setFile('brand.tpl.php')
         ->setFile('home.tpl.php')
         ->setFile('footer.tpl.php');  
  }
  
}
// EOF 