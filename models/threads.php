<?php 
namespace miniboard\Models;

use miniboard\includes\Models, 
    \PDO, \Exception; 
    



/** The ThreadsDbLayer class is a wrapper class to deal with threads records 
  * datas in database. 
  *
  * +-----------+---------+------+-----+---------+----------------+
  * | Field     | Type    | Null | Key | Default | Extra          |
  * +-----------+---------+------+-----+---------+----------------+
  * | id        | int(11) | NO   | PRI | NULL    | auto_increment |
  * | owner     | int(11) | NO   |     | NULL    |                |
  * | thread_id | int(11) | NO   |     | NULL    |                |
  * | content   | text    | NO   |     | NULL    |                |
  * | timestamp | int(11) | NO   |     | NULL    |                |
  * +-----------+---------+------+-----+---------+----------------+
  *
  * @author  : Thomas LHTS <thomas.lhts[at]protonmail.ch>
  * @date    : 2019.07
  * @license : https://opensource.org/licenses/MIT
  * @version : 0.1a  */
  
class ThreadsDbLayer extends Models {


  /** Involved tables names are kept here for maintainability concerns.
    * @var $tables */
  protected $tables = [ 'threads'   =>   'threads',  
                        'posts'     =>   'posts',  
                        'users'     =>   'users',
                        'profiles'  =>   'users_profiles' ];
                        
            
  /** This array contains values to be used for the next SQL query.
    * It has to be resetted between each query. 
    * @var array $values */               
  protected $values;


  /** Is used for injection of dependencies.
    * @var array $injected */
  protected $injected; 


  /** The last thread inserted id gets updated each time the ThreadsDbLayer::fetch() 
    * method is called. 
    * @var int $lastInsertId */
  protected $lastInsertId = 0;
  
  // + + + + + + + + + +  




  public function __construct() {}


  /** Return the last thread insert id in database or 0 if no thread has been 
    * inserted yet. 
    * @return int */
  public function lastInsertId() {
    return $this->lastInsertId;
  }
    
  // + + + + + 




  /** Save a thread in database.
    * @return bool */
    
  public function save() : bool {
  
    $this->values['title'] = trim($this->values['title']);
  
    if(!isset($this->values['id'])):
  
      // Insert a new thread's metadatas 
      $r = $this->injected['dbh']->prepare('INSERT INTO ' . $this->tables['threads'] . ' VALUES(NULL, :owner, :title, :start, :locked, :pinned, :timestamp)');
      
      $r->bindValue(':owner', $this->values['owner'], PDO::PARAM_INT); 
      $r->bindValue(':title', $this->values['title'], PDO::PARAM_STR);
      $r->bindValue(':start', $this->values['start'], PDO::PARAM_INT);
      $r->bindValue(':locked', $this->values['locked'], PDO::PARAM_INT);
      $r->bindValue(':pinned', $this->values['pinned'], PDO::PARAM_INT);
      $r->bindValue(':timestamp', time(), PDO::PARAM_INT);    
        
      $this->values = [];  
      $d = $r->execute(); 
      $this->lastInsertId = (int) $this->injected['dbh']->lastInsertId(); 

      return $d;
   
    else:
    
      // Update a thread's metadatas 
      $r = $this->injected['dbh']->prepare('UPDATE ' . $this->tables['threads'] . ' SET title = :title WHERE id = :id');
      $r->bindValue(':title', $this->values['title'], PDO::PARAM_STR);
      $r->bindValue(':id', $this->values['id'], PDO::PARAM_STR);
      
      $this->values = [];
      $d = $r->execute();
      $r->closeCursor();
      
      return $d;
    endif;
  }



  /** To make the ThreadDbLayer::fetch() SELECT query SQL code reusable to make declinaisons.
    * @return string */
    
  private function _fetch_query($whereClause = '', $limitClause = '') {
    
    return sprintf('SELECT %1$s.id, %1$s.title, %1$s.locked, %1$s.timestamp, %2$s.id AS first_post_id, %2$s.content, %2$s.owner, ' 
                            . '%3$s.username, crafted.last_reply, %4$s.picture ' 
                     
                 . 'FROM %1$s ' 
            
                 // Last post's timestamp 
                 . 'LEFT JOIN '
                      . '(SELECT thread_id, MAX(timestamp) AS last_reply FROM %2$s GROUP BY %2$s.thread_id) ' 
                      .       ' AS crafted ON crafted.thread_id = %1$s.id '
                 
                 . 'LEFT JOIN %2$s ON %2$s.id = %1$s.start '
                 . 'LEFT JOIN %3$s ON %3$s.id = %2$s.owner ' 
                 . 'LEFT JOIN %4$s ON %4$s.user_id = %2$s.owner%5$s '
                 . 'ORDER BY crafted.last_reply DESC%6$s',
                    $this->tables['threads'], $this->tables['posts'], $this->tables['users'], $this->tables['profiles'], $whereClause, $limitClause);   

  }




  /** Fetch the thread list from database or a thread specified by its id.
    * @return array */
    
  public function fetch() : array {
    
    $whereClause = '';
    if(isset($this->values['id']))
      $whereClause = sprintf(' WHERE %s.id = :id', $this->tables['threads']);
      
    if(!empty($whereClause)) 
       $whereClause .= ' AND';
       
    else $whereClause = ' WHERE ';
    
    $whereClause .= sprintf(' %s.pinned = 0', $this->tables['threads']); // does not retrieve pinned posts 
    $limitClause = $this->paginate($this->tables['threads']);

    // Craft a fetch query          
    $r = $this->injected['dbh']->prepare($this->_fetch_query($whereClause, $limitClause));
        
    if(isset($this->values['id']))
      $r->bindValue(':id', $this->values['id'], PDO::PARAM_INT);
    
    $d = $r->execute();
    if(!$d) return []; 
    
    $res = isset($this->values['id']) && $this->values['id'] ? $r->fetch(PDO::FETCH_ASSOC):$r->fetchAll(PDO::FETCH_ASSOC);
  
    $this->values = [];
    $r->closeCursor();
    return $res;
  }




  /* Fetch the list of pinned threads */
  public function pinned() : array  {

      $r = $this->injected['dbh']->query($this->_fetch_query(' WHERE pinned = 1'));
      
      if(!$r) return false;
      $res = $r->fetchAll(PDO::FETCH_ASSOC);
            
      $r->closeCursor();
      return $res;
  }




  /** Delete the specified thread(s) and the related posts. 
    * @return bool */
    
  public function delete() : bool {

    if(empty($this->values['id'])):
      $this->values = [];
      return false;
    endif;

    if(isset($this->values['id'])):
      if(is_array($this->values['id'])):
        
        $threadsWhereClause = $postsWhereClause = ' WHERE';
        foreach($this->values['id'] as $k => $id):
          $threadsWhereClause .= ' id = ' . intval($id);
          $postsWhereClause .= ' thread_id = ' . intval($id);
          
          if(isset($this->values['id'][($k + 1)])):
            $threadsWhereClause .= ' OR';
            $postsWhereClause .= ' OR';
          endif;
         endforeach;
        
      else:
        $threadsWhereClause = ' WHERE id = ' . intval($this->values['id']);
        $postsWhereClause = ' WHERE thread_id = ' . intval($this->values['id']);
      endif;
            
      // Delete selected thread(s)
      $r = $this->injected['dbh']->exec('DELETE FROM ' . $this->tables['threads'] . $threadsWhereClause);
      if(!$r) return false;
      $this->values = [];
      
      // Delete all related posts 
      $r = $this->injected['dbh']->exec('DELETE FROM ' . $this->tables['posts'] . $postsWhereClause);
      
      if(!$r) return false;
      
      return true;
      
    endif;
    return false;
  }
  
  // + + + + + + + + + + +




  /** Set the first post id of the specified thread.
    * @return bool */
  public function setStartId() : bool {
    
    if((isset($this->values['start']) && $this->values['start']) && isset($this->values['id']) && $this->values['id']) {
      
      $r = $this->injected['dbh']->prepare('UPDATE ' . $this->tables['threads'] . ' SET  start = :start WHERE id = :id');
      
      $r->bindValue(':start', (int) $this->values['start'], PDO::PARAM_INT);
      $r->bindValue(':id', (int) $this->values['id'], PDO::PARAM_INT);
      
      $d = $r->execute();
      $this->values = [];
      $r->closeCursor();
      return $d;
      
    }
    
    return false;
  }




  /** Get the list of posts that belong to the specified thread.
    * @return array */
    
  public function getPosts() : array {
      
    if(isset($this->values['id'])):
      
      $limitClause = $this->paginate($this->tables['posts'], sprintf('%s.thread_id = %d', $this->tables['posts'], (int) $this->values['id']));
      
      $SQL = sprintf('SELECT %1$s.id, %1$s.owner, %1$s.thread_id, %1$s.content, %1$s.timestamp, %2$s.title, %2$s.locked, %3$s.username, %4$s.picture '
                   . 'FROM %1$s ' 
                   . 'LEFT JOIN %2$s ON %2$s.id = %1$s.thread_id ' 
                   . 'LEFT JOIN %3$s ON %3$s.id = %1$s.owner '
                   . 'LEFT JOIN %4$s ON %4$s.user_id = %1$s.owner '
                   . 'WHERE %2$s.id = :id ORDER BY %1$s.timestamp %5$s',
                     $this->tables['posts'], $this->tables['threads'], $this->tables['users'], $this->tables['profiles'], $limitClause);    
      
      
      $r = $this->injected['dbh']->prepare($SQL);
      $r->bindValue(':id', $this->values['id'], PDO::PARAM_INT);
      $d = $r->execute();
      
      if(!$d) return []; 
      $res = $r->fetchAll(PDO::FETCH_ASSOC);
        
      $this->values = [];
      $r->closeCursor();
      return $res;
      
    endif;
    
    return [];
    
  }
  
  // + + + + + + + + + +




  /** Change threads locked status.
    * @param bool $unlock Wether we unlock or lock the specified threads. 
    * @return bool */
  public function lock($unlock = false) : bool {
    
    if(empty($this->values['id'])):
      $this->values = [];
      return false;
    endif;
    
    if(!isset($this->values['id'])) return false;
    
    $whereClause = '';
    
    // Several ids values has been specified as an array 
    if(is_array($this->values['id']) && !empty($this->values['id'])):
      $whereClause  = ' WHERE ';
      foreach($this->values['id'] as $k => $id):
        
        if(!is_int($id) && !(is_numeric($id) && intval($id)))
          throw new Exception(__METHOD__ . ' : Invalid `id` value supplied.');
        
        $whereClause .= 'id = ' . intval($id);
        
        if(isset($this->values['id'][($k + 1)]))
          $whereClause .= ' OR ';
      endforeach;
      
    // Only one value has been specified as integer 
    elseif(is_int($this->values['id'])):
      $whereClause = ' WHERE id = ' . intval($this->values['id']);
    
    else: 
      return false;
    endif;
    
    $SQL = 'UPDATE ' . $this->tables['threads'] . ' SET locked = ' . ($unlock ? 0:1) . $whereClause;
    
    $r = $this->injected['dbh']->exec($SQL);
    $this->values = [];
    return $r;
    
  }




  // Figure out wether a thread is locked or not 
  
  public function isLocked() : bool {
    
    if(isset($this->values['id'])):
      
      $r = $this->injected['dbh']->prepare('SELECT locked FROM ' . $this->tables['threads'] . ' WHERE id = :id');
      $r->bindValue(':id', $this->values['id'], PDO::PARAM_INT);
      $d = $r->execute();

      if(!$d)
        throw new Exception(__METHOD__ . ' - unknown error.');
          
      $res = $r->fetch(PDO::FETCH_ASSOC);
      if(empty($res)) exit(); // looks like thread doesn't exist
      
      return (bool) $res['locked'];
      
    else: 
      throw new Exception(__METHOD__ . ' - no id value specified.');
    endif;
    
  }

  // + + + + + + + + + + + 


  /** Pin or unpin the specified thread 
    * @return bool */
    
  public function pin($unpin = false) : bool {
    
    if(empty($this->values['id'])):
      $this->values = [];
      return false;
    endif;
      
    $whereClause = 'WHERE ';
    
    if(is_array($this->values['id'])): // WHERE clause with several thread ids specified 
      foreach($this->values['id'] as $k => $ids):
        
        $whereClause .= ' id = ' . intval($ids);
        if(isset($this->values['id'][($k + 1)]))
          $whereClause .= ' OR ';  
      endforeach;
      
    else:
      $whereClause .= 'id = ' . intval($this->values['id']); // signle thread id specified
    endif;
      
    $r = $this->injected['dbh']->query(sprintf('UPDATE %s SET pinned = %d %s', $this->tables['threads'], ($unpin ? 0:1), $whereClause));
    $this->values = [];
    return ($r != false ? true:false);
  } 



  // + + + + + + + + + + +  


  /** To INSERT new posts in the database the more optimized way possible we rely 
    * on the PDO::lastInsertId() method : Sometimes, in rare cases, lastInsertId() fails 
    * to return a value and this will lead the involved thread display to an error. 
    * A thread is called orphan when no related post can be found in the posts table ; 
    * which in theory is a impossible situation because all threads must to have at least 
    * one post. */
    
  public function cleanOprhans() : void {
    
    $r = $this->injected['dbh']->query('SELECT id FROM ' . $this->tables['threads'] . ' WHERE start = 0');
    
    if(!$r == false):
      $res = $r->fetchAll(PDO::FETCH_ASSOC);
      $r->closeCursor();
          
      if(!empty($res)): // if there is at least one orphan node, craft a query to delete them
        $whereClause = 'WHERE';
        foreach($res as $k => $rec):
          $whereClause .= ' id = ' . intval($rec['id']);
          if(isset($res[($k + 1)]))
            $whereClause .= ' AND';
        endforeach;
        
        $r = $this->injected['dbh']->exec('DELETE FROM ' . $this->tables['threads'] . ' ' . $whereClause);
        $r->closeCursor();
        
      endif;
    endif;
  }
  
  // + + + + + + + + + + + 




  // Figure out wether the specified thread is owned by the specified user, or not.
  
  public function isOwnThread() : bool {
    
    if(isset($this->values['id'])):
      
      $r = $this->injected['dbh']->query('SELECT owner FROM ' . $this->tables['threads'] . ' WHERE id = ' . intval($this->values['id']));
      if(!$r) return false;
      $res = $r->fetch(PDO::FETCH_ASSOC);
    
      if(empty($res)) return false;
      $r->closeCursor();
      
      $userId = (isset($this->values['user_id']) ? intval($this->values['user_id']):0);
      if(!$userId):
        $userId = (isset($_SESSION['id']) ? $_SESSION['id']:0); // try to use current logged-in user id.
        if(!$userId) // a guest user doesn't own any thread 
          return false;
      endif;
      
      return ($res['owner'] == $userId) ? true:false;
        
    else:
      throw new Exception(__METHOD__  . ' : you must to specify a thread id.');  
    endif;
    
  }

}
// EOF