<?php
namespace miniboard\Models;

use miniboard\includes\Models,
    \PDO, \Exception;
  


  
/** The Config model to deal with the platform's general configuration.
  *
  * Table structure :  
  *
  * +---------+--------------+------+-----+---------+----------------+
  * | Field   | Type         | Null | Key | Default | Extra          |
  * +---------+--------------+------+-----+---------+----------------+
  * | id      | int(11)      | NO   | PRI | NULL    | auto_increment | 
  * | keyname | varchar(255) | NO   |     | NULL    |                |
  * | value   | varchar(255) | NO   |     | NULL    |                |
  * +---------+--------------+------+-----+---------+----------------+    
  *
  * Default values to be inserted to work with this model, as the first 
  * created account has to be both administrator & moderator by default.
  *
  * +----+----------------+-----------------+
  * | id | keyname        | value           |
  * +----+----------------+-----------------+
  * |  1 | moderators     | [1]             |
  * |  2 | administrators | [1]             |
  * +----+----------------+-----------------+
  *
  * @author  :  Thomas LHTS <thomas.lhts@protonmail.ch>
  * @date    :  2019.07
  * @license :  https://opensource.org/licenses/MIT
  * @version :  0.1a                                                            */


class ConfigDbLayer extends Models {
  
  protected $tables = [ 'config'  =>  'config' ];
    

  public function __construct($dbh = NULL) {
    
    if($dbh instanceof PDO):
       $this->PDO =& $this->injected['dbh'];
       $this->injected['dbh'] = $dbh;
       
       // Fetch all configuration values from the database
       $this->fetch();

    endif;
    
  }
  
  // + + + + + + + + + + 
  
  
  
  
  // Dependency injection helper 
  
  public function &inject($key, $value) : object {
    $this->injected[$key] = $value;
    
    if($value instanceof PDO) // database handler has been injected
       $this->fetch();
       
    return $this;
  }
  
  // + + + + + + + + + + +
  
  
  
  
  // Getters and setters 
  
  
  /** Somehow a general-purpose setter to write configuration values to database.
    * Notice the configuration table gets updated each time this method is called. 
    *
    * @param string $key The key to associate the data with in the ArticlesModel::$values array. 
    * @param mixed $value The value to be associated to the specified key. 
    * @return self The object itself. */
    
  public function &set($key, $value) {
    [$key, $value] = [trim($key), trim($value)];
    $this->values[$key] = $value;

        
    $r = $this->injected['dbh']->prepare('SELECT id, keyname, value FROM ' . $this->tables['config'] . ' WHERE keyname = :key');  
    $r->bindValue(':key', $key, PDO::PARAM_STR);
    $d = $r->execute();
      
    if($d):
      
      $res = $r->fetch(PDO::FETCH_ASSOC);
      if(empty($res)):
        
        $r = $this->injected['dbh']->prepare('INSERT INTO ' . $this->tables['config'] . ' VALUES(NULL, :key, :value)');
        $r->bindValue(':key', $key, PDO::PARAM_STR);
        $r->bindValue(':value', $value, PDO::PARAM_STR);
        $d = $r->execute();
        $r->closeCursor();
      
      else:
        
        $r = $this->injected['dbh']->prepare('UPDATE ' . $this->tables['config'] . ' SET value = :value WHERE keyname = :key');
        $r->bindValue(':value', $value, PDO::PARAM_STR);
        $r->bindValue(':key', $key, PDO::PARAM_STR);
        $d = $r->execute();
        $r->closeCursor();
        
      endif;
      
    endif;    
    return $this;
  }
  
  
  
  
  /** Somehow a general purpose getter to get configuration values from the database. */
  
  public function get($key) {
    
    // This values has already been fetched (mostly at instanciation)
    if(isset($this->values[$key])):
       return $this->values[$key];
       
    // Attempting to fetch the related value from the database.
    else:
      
      $r = $this->injected['dbh']->prepare('SELECT id, keyname, value FROM ' . $this->tables['config'] . ' WHERE keyname = :key');
      $r->bindValue(':key', $key, PDO::PARAM_STR);
      $d = $r->execute();
      
      if(!$d):
        $res = $r->fetch(PDO::FETCH_ASSOC);
        $r->closeCursor();
        
        if(empty($res)) 
          return false;  
        else
          return $res['value'];
      endif;    
    endif;
  }  
  
  // + + + + + + + + + + 
  
  
  

  /** Fetch all configuration keys & values from the database. */
  
  public function fetch() {
    
    $r = $this->injected['dbh']->query('SELECT id, keyname, value FROM ' . $this->tables['config']);
    if($r):
      $res = $r->fetchAll(PDO::FETCH_ASSOC);
      $r->closeCursor();
      
      foreach($res as $rec)
         $this->values[$rec['keyname']] = $rec['value'];
    endif;
        
  }
  
  // Have a look to the Config::set() general setter to change values. 
  
  
  // + + + + + + + + + +
  


  
  // Determine the specified user special rights using predefined configuration value.
  
  
  public function isModerator(int $userId = 0, int $setter = 0, bool $drop = false) : bool {
    
    // The predefined configuration field ' moderator ' is a JSON serialized string that  contains the moderators ids. 
    if(isset($this->values['moderators']))
       $moderatorsArr = json_decode($this->get('moderators'), true);
    else
      throw new Exception(__METHOD__ . ' : Can\'t find predefined `moderators` configuration option row in database.');
    
    // If no user id has been specified ; we try to use the current session's id instead
    $userId = !$userId ? (isset($_SESSION['id']) ? $_SESSION['id']:0):$userId;
    
    // Is a guest user ? 
    if(!$userId) return false;
    
    // In `setter` mode, it's necessary to change the specified user status 
    if($setter):
      
      // Add the moderator status to the specified user 
      if(!$drop):
                
        if(!in_array($userId, $moderatorsArr)):
          $moderatorsArr[] = $userId;
          $moderatorsArr = array_values($moderatorsArr); // array reindexation 
          $this->set('moderators', json_encode($moderatorsArr));
        endif;
        
      // Remove the moderator status to the specified user 
      else:     
                     
        if(($i = array_search($userId, $moderatorsArr)) !== false):
          
          unset($moderatorsArr[$i]);
          $moderatorsArr = array_values($moderatorsArr); // array reindexation 
          $this->set('moderators', json_encode($moderatorsArr));
           
        endif;  
        
        
      endif;
    
    endif;

    // Is the user represented by the $userId variable a moderator ? 
    $isModerator = $userId ? (int) in_array($userId, $moderatorsArr):0;
    return $isModerator;
  }
  
  
  public function isAdministrator(int $userId = 0, int $setter = 0, bool $drop = false) : bool {
    
    // The predefined configuration field ' administrator ' is a JSON serialized string that contains the administrators' ids. 
    if(isset($this->values['administrators']))
      $administratorsArr = json_decode($this->get('administrators'), true);
    else
      throw new Exception(__METHOD__ . ' : Can\'t find predefined `administrators` configuration option row in database.');
    
    // If no user id has been specified ; we try to use the current session's id instead
    $userId = !$userId ? (isset($_SESSION['id']) ? $_SESSION['id']:0):$userId;
    
    // Is a guest user ? 
    if(!$userId) return false;
    
    // In `setter` mode, it's necessary to change the specified user status 
    if($setter):
      
      // Add the moderator status to the specified user 
      if(!$drop):
        if(!in_array($userId, $administratorsArr)):
          $administratorsArr[] = $userId;
          $administratorsArr = array_values($administratorsArr); // array reindexation 
          $this->set('administrators', json_encode($administratorsArr));
        endif;
        
      // Remove the moderator status to the specified user 
      else:          
        if($i = array_search($userId, $administratorsArr) !== false):
          
          unset($administratorsArr[$i]);
          $administratorsArr = array_values($administratorsArr); // array reindexation 
          $this->set('administrators', json_encode($administratorsArr));
        endif;  
      endif;
    
    endif;
    
    // Is the user represented by the $userId variable an administrator ? 
    $isAdministrator = $userId ? (int) in_array($userId, $administratorsArr):0;
    return $isAdministrator;    
  }
  
}

// EOF 