<?php 
namespace miniboard\Models;


use miniboard\includes\Models,
    \Exception;




/** Basic upload script 
 *
 *  The purpose of this file is to process the image uploads request sent by the 
 *  input.js client script. It has to double check the file and to move them to 
 *  the server if necessary. 
 *
 *  @author  : Thomas LHTS <thomas.lhts[at]protonmail.ch>
 *  @date    : 2019.14.07 
 *  @version : 0.1a                                                           */ 
class ImageUpload extends Models {


  private $uploadDirectory = 'uploads/';


  /** A callback function may be defined here to be called each time a file has been uploaded */
  private $callback = null;




  public function __construct($uploadDirectory = null) {

    // Possible overwrite of the upload directory
    if($uploadDirectory):
      $uploadDirectory = ($uploadDirectory[strlen($uploadDirectory) - 1] == '/') ? $uploadDirectory:($uploadDirectory . '/');
      $this->uploadDirectory = $uploadDirectory;   
    endif;
    
  }




  public function upload(string $blob) {       
    if(empty($blob)) return false;

    // Create image from blob
       
    $base64 = base64_decode(explode(',', $blob)[1]); // need to drop 'data:image/png;base64' part
    if(getimagesizefromstring($base64) == false) // ensure this is a proper image
       throw new Exception('Invalid image blob given.');
       
    $currentImage = imagecreatefromstring($base64); // create image from base64
    [$originalWidth, $originalHeight] = getimagesizefromstring($base64); // get original image width & height
    
    
    // Width & height
    
    $width = isset($this->values['width']) ? $this->values['width']:$originalWidth;
    $height = isset($this->values['height']) ? $this->values['height']:$originalHeight;



    // When it comes to scale images, reproduces the CSS behaviour when only a width OR a height value is specified.
      
    if(!empty($this->values['width']) && empty($this->values['height'])):
      
      // If a width value is specified, but no height value
      if($originalWidth > $this->values['width']):
        $widthRatio = (100 * ($originalWidth - $this->values['width']) / $originalWidth); // compute the ratio between the two sizes 
        $height = ($originalHeight - (($originalHeight * $widthRatio) / 100)); // remove the same ratio to the height
      else:
        return false; // does not fit the minimum size requirement
      endif;
    
    elseif(empty($this->values['width']) && !empty($this->values['height'])):
      
      // If a height value is specified, but no width value
      if($originalWidth > $this->values['width']):
        $heightRatio = (100 * ($originalHeight - $this->values['height']) / $originalHeight); // compute the ratio between the two sizes 
        $height = ($originalHeight - ($originalHeight * $ratio) / 1); // remove the same ratio to the width
      else:
        return false; // does not fit the minimum size requirement
      endif;
    endif;

    $tmp = imagecreatetruecolor($width, $height);
    $forceAlpha = imagecolorallocatealpha($tmp, 0, 0, 0, 127); // force alpha layer
    imagecopyresampled($tmp, $currentImage, 0, 0, 0, 0, $width, $height, $originalWidth, $originalHeight); // resample image


    // Create image file on server

    do 
      // generate a avaliable-on-server filename
      $filename = $this->uploadDirectory . time() * rand(0, 100) . rand(0, 10000) . '.png'; 
      
    while(is_file($filename));
    imagepng($tmp, $filename); // place image right on the server 
    return $filename; // return the file's path
    
  }
  
}
// EOF