<?php 
namespace miniboard\Models;

use miniboard\includes\Models,
    \PDO, \Exception; 

/** The PostsDbLayer class is a wrapper class to deal with posts records 
  * datas in database. 
  *
  * +-----------+---------+------+-----+---------+----------------+
  * | Field     | Type    | Null | Key | Default | Extra          |
  * +-----------+---------+------+-----+---------+----------------+
  * | id        | int(11) | NO   | PRI | NULL    | auto_increment |
  * | owner     | int(11) | NO   |     | NULL    |                |
  * | thread_id | int(11) | NO   |     | NULL    |                |
  * | content   | text    | NO   |     | NULL    |                |
  * | timestamp | int(11) | NO   |     | NULL    |                |
  * +-----------+---------+------+-----+---------+----------------+
  *
  * @author  : Thomas LHTS <thomas.lhts[at]protonmail.ch>
  * @date    : 2019.07
  * @license : https://opensource.org/licenses/MIT
  * @version : 0.1a */
  
class PostsDbLayer extends Models {
  
  /** Involved tables names are kept here for maintainability concerns.
    * @var $tables */
    
  protected $tables = [ 'threads' => 'threads', 'posts' => 'posts'];


  /** This array contains values to be used for the next SQL query.
    * It has to be resetted between each query. 
    * @var array $values */ 
  protected $values;
  
  /** Is used for injection of dependencies.
    * @var array $injected */
  protected $injected; 


  /** The last thread inserted id gets updated each time the ThreadsDbLayer::fetch() 
    * method is called. 
    * @var int $lastInsertId */
  public $lastInsertId = null;
  
  
  public function __construct() {}




  /** The last thread inserted id gets updated each time the ThreadsDbLayer::fetch() 
    * method is called. 
    * @var int $lastInsertId */    
  
  public function lastInsertId() {
    return $this->lastInsertId;
  }
    
  // + + + + + 
  
  

  
  /** Save a new post record to the database. 
    * @return bool */
  
  public function save() : bool {

    $this->values['content'] = trim($this->values['content']);
    
    if(!$this->values['content'])
       return false;
  
    if(!isset($this->values['id']) || !(int) $this->values['id']):
  
      // Insertion  
      $r = $this->injected['dbh']->prepare('INSERT INTO ' . $this->tables['posts'] . ' VALUES(NULL, :owner, :thread_id, :content, :timestamp)');
      
      $r->bindValue(':owner', $this->values['owner'], PDO::PARAM_INT);
      $r->bindValue(':thread_id', $this->values['thread_id'], PDO::PARAM_INT);
      $r->bindValue(':content', $this->values['content'], PDO::PARAM_STR);
      $r->bindValue(':timestamp', time(), PDO::PARAM_INT);
      
      $d = $r->execute();
      $r->closeCursor();
      
      $this->lastInsertId = (int) $this->injected['dbh']->lastInsertId();    
      return $d;
      
    else:
      
      // Update 
      $r = $this->injected['dbh']->prepare('UPDATE ' . $this->tables['posts'] . ' SET content = :content WHERE id = :id');
      $r->bindValue(':content', $this->values['content'], PDO::PARAM_STR);
      $r->bindValue(':id', $this->values['id'], PDO::PARAM_INT);
      $d = $r->execute();
      $r->closeCursor();

      return $d;      
    endif;
  }




  /** Fetch posts records from the database. 
    * @return array Selected records. */
    
  public function fetch() : array {
    
    $whereClause = '';
    
    // Take care of a possible thread.id filter.
    if(!empty($this->values['id'])):
       $whereClause = sprintf(' WHERE %1$s.id = :id', $this->tables['posts']); 
    endif;
    
    $SQL = 'SELECT id, owner, thread_id, content, timestamp FROM ' . $this->tables['posts'] . $whereClause;
    $r = $this->injected['dbh']->prepare($SQL);
    
    if(!empty($this->values['id'])):
      $r->bindValue(':id', $this->values['id'], PDO::PARAM_STR);
    endif; 
        
    $d = $r->execute();
    if(!$d) return [];
    
    $res = (!empty($this->values['id'])) ? $r->fetch(PDO::FETCH_ASSOC):$r->fetchAll(PDO::FETCH_ASSOC);
    $r->closeCursor(); 
    return !empty($res) ? $res:[];
  }
  
  
  
    
  /** Delete post records from database 
    * @return bool */
  public function delete() : bool {
    
    if(isset($this->values['id'])):
      
      if(is_int($this->values['id'])):
        $whereClause = ' WHERE id = ' . intval($this->values['id']);
        
      elseif(is_array($this->values['id'])):
        $whereClause = ' WHERE ';
        
        foreach($this->values['id'] as $k =>  $id):
          $whereClause .= ' id = ' . intval($id);
          if(isset($this->values['id'][($k + 1)]))
            $whereClause .= ' OR ';
        endforeach;
      endif;
      
      $r = $this->injected['dbh']->exec('DELETE FROM ' . $this->tables['posts'] . $whereClause);
      $this->values = [];
      
      return true;
    endif;
    
    return false;
  }
  
  // + + + + + + + + + + 

  
  
  
  /** Determine if the specified post belongs to the specified user. 
    * @return bool */

  public function isOwnPost() : bool {
    
    if(isset($this->values['id'])):
      
      $userId = (isset($this->values['user_id']) ? intval($this->values['user_id']):0);
      if(!$userId):
        $userId = (isset($_SESSION['id']) ? $_SESSION['id']:0); // try to use current logged-in user id.
        if(!$userId) // a guest user doesn't own any thread 
          return false;
      endif;
      
      $r = $this->injected['dbh']->query('SELECT owner FROM ' . $this->tables['posts'] . ' WHERE id = ' . intval($this->values['id']));
      if(!$r) 
          return false;
      
      $res = $r->fetch(PDO::FETCH_ASSOC);
      if(!$res) 
          return false; 
      
      return ($res['owner'] == $userId) ? 1:0;
      
    else:
      throw new Exception(__METHOD__ . ' : You must to specify a post id.');
    endif;
    
  }
  
  // + + 
  
  
  /** This method ensure the post specified by its id is not the starting post 
    * of any thread. We use this method to prevent threads' first messages 
    * delete that would cause the thread's table informations to be corrupted.
    * This method relies on the value of the PostsDbLayer::$values['id'] variable. */
  
  public function isRemovable() : bool {
    
    if(!empty($this->values['id'])):
      
      // Count the number of threads whose the specified post id is the starting post.
      $r = $this->injected['dbh']->prepare(sprintf('SELECT COUNT(id) AS nbr FROM %s WHERE start = :id', $this->tables['threads']));
            
      $r->bindValue(':id', $this->values['id'], PDO::PARAM_INT);
      $r->execute();
      
      if(!$r)
        return false;
        
      $res = $r->fetch(PDO::FETCH_ASSOC);
      $r->closeCursor();
            
      return $res['nbr'] ? false:true;  
    endif;
    
    throw new Exception(__METHOD__ . ' : You must to specify a post id.');
  }
  
}
// EOF