<?php
/**                          HANDLE USERS DATASETS
 *
 *   @author : Thomas LHTS <thomas.lhts[at]protonmail.ch>
 *   @started : 2018.06.07
 *   @license : https://opensource.org/licenses/MIT
 *   @version : 0.1a
 * 
 *   +-----------+--------------+------+-----+---------+----------------+
 *   | Field     | Type         | Null | Key | Default | Extra          |
 *   +-----------+--------------+------+-----+---------+----------------+
 *   | id        | int(11)      | NO   | PRI | NULL    | auto_increment |
 *   | username  | varchar(255) | NO   |     | NULL    |                |
 *   | email     | varchar(255) | NO   |     | NULL    |                |
 *   | password  | varchar(255) | NO   |     | NULL    |                |
 *   | timestamp | int(11)      | NO   |     | NULL    |                |
 *   +-----------+--------------+------+-----+---------+----------------+
                                                                            */

namespace miniboard\Models; 

use  miniboard\includes\Models,
     \Exception, \PDO;

class Users extends Models {


  /** @var String $table String which represents the table where to store permissions' metadatas sets. */
  public $tables = ['users'     => 'users',
                    'profiles'  => 'users_profiles'];


  /** @var Permissions $Permissions Permissions object dependancy injection.*/
  private $Permissions;

  /** @var UsersHistory $UsersHistory UsersHistory object. */
  private $UsersHistory = NULL;




  /** @var Array $regexp Array of used regexp to validate users inputs. */
  private $regexp = [ 'username' => '#^[a-z0-9_-]{3,15}$#i', 
                      'email' => '#^[A-Z0-9._+-]+@[A-Z0-9.-]+\.[A-Z]{2,}#i', 
                      'password' => '#^(.+){8,}$#i']; // @todo: some additional tests to match alphanum + at least special character RIGHT DOWN in the password mutator.




  /*                              ERRORS CODES                                */
  
  const USERNAME_FORMAT_ERROR = 101;  
  
  const USERNAME_UNAVALIABLE = 105;    
  
  const EMAIL_FORMAT_ERROR = 102;
  
  const PASSWORD_FORMAT_ERROR = 103;   
   
  const PASSWORDS_MISMATCH = 104;     
   
  const LOGIN_ERROR = 106;




  /**  @desc Performs new instance configuration
   *  @param $PDO PDO PDO dependancy  injection
   *  @return Users The instance itself.  */
   
  function __construct() {}
  

  

  /**                           PASSWORDS HASHES
   *
   *    @desc Handles password hashes. If no hashes is given, then this method returns a hashed password. If a hash is given, then the password
   *    is encrypted and compared to the hash. The result of this comparaison is returned by the method.
   *    @param String $val A clear-text password given by the user.
   *    @param String $anotherHash A hash to compare the password with if we want to validate a password.
   *    @return String|Bool A hashed password or the result of a passwords comparison. */
   
  public function password($val, $anotherHash = NULL) {
     return ($anotherHash == NULL) ? password_hash($val, PASSWORD_BCRYPT):password_verify($val, $anotherHash); 
  }




  /**                        SAVE DATAS IN DATABASE
   *
   *    @desc This made a request to save a new record in database or update an existing one based on the class' properties values.
   *    @param  Bool $skipPassword Determine if we need to skip password update or not (like in administration context).
   *    @return Integer The query's result    */
   
  function save($skipPassword = false) {
    
    // Values format 
    if(!preg_match($this->regexp['username'], $this->values['username']))  
        return Users::USERNAME_FORMAT_ERROR; 
    
    if(!preg_match($this->regexp['email'], $this->values['mail']))  
        return Users::EMAIL_FORMAT_ERROR;


    $skipPassword = (bool) $skipPassword; // as password verification might be skipped for an account update.

    // Ensure password & confirmation matches 
    if(!isset($this->values['password']) || !isset($this->values['passwordConfirmation']) ||
          ($this->values['password'] != $this->values['passwordConfirmation'])):
          
      if(!$skipPassword)
        return Users::PASSWORDS_MISMATCH; // in " skip password " mode this doesn't make the script to fail.
      else 
        $this->values['password'] = NULL;
        
    endif;
    
  
  
    if(!preg_match($this->regexp['password'], $this->values['password']))
      // Password format error 
      if(!$skipPassword):
        return Users::PASSWORD_FORMAT_ERROR;
      endif;
    else
      // Hashing password 
      $this->values['password'] = $this->password($this->values['password']); 



    if(!isset($this->values['id']) || !$this->values['id']): 
      
      // Insert a new record  
      
      // Checking the given user is avaliable 
      $r = $this->injected['dbh']->prepare('SELECT COUNT(id) AS nbr FROM ' . $this->tables['users'] . ' WHERE username = :username');
      $r->bindValue(':username', $this->values['username'], PDO::PARAM_STR); 
      $d = $r->execute(); 
      if(!$d) return $d;
      
      $res = $r->fetch(PDO::FETCH_ASSOC); // if the value is prior than 1 it means the username isn't avaliable 
      $r->closeCursor(); 
      
      // Can't continue because username is unavaliable
      if($res['nbr']) 
        return Users::USERNAME_UNAVALIABLE; 
      
      $r = $this->injected['dbh']->prepare('INSERT INTO ' . $this->tables['users'] . ' VALUES(NULL, :username, :email, :password, :time)'); 
      $r->bindValue(':username', $this->values['username'], PDO::PARAM_STR); 
      $r->bindValue(':email', $this->values['mail'], PDO::PARAM_STR); 
      $r->bindValue(':password', $this->values['password'], PDO::PARAM_STR); 
      $r->bindValue(':time', time(), PDO::PARAM_INT);
      $d = $r->execute(); 
      $r->closeCursor(); 
      $this->values = [];
      return $d;

    else: 
      
      // Update a existing record  
      
      $r = $this->injected['dbh']->prepare('UPDATE ' . $this->tables['users'] . ' SET username = :username,' . ((isset($this->values['password']) && $this->values['password'] != NULL) ? ' password = :password,':'') . ' email = :email WHERE id = :id'); 
      $r->bindValue(':username', $this->values['username'], PDO::PARAM_STR); 
      $r->bindValue(':email', $this->values['mail'], PDO::PARAM_STR); 
      $r->bindValue(':id', $this->values['id'], PDO::PARAM_INT);
      
      if($this->values['password']) 
        $r->bindValue(':password', $this->values['password'], PDO::PARAM_STR);
        
      $d = $r->execute(); 
      $r->closeCursor(); 
      $this->values = [];
      return $d;
    endif;
  }




  /**                        GET RECORDS FROM DATABASE
   *
   *    @desc Returns users' records from database. If a value greater than 0 is specified for the `$this->id` property, the method will try to return
   *          this speficif record. The other case, it will return all the records
   *    @return Array|String Array of records from database or false if an error happenned. */
   
  function fetch() {
    $whereClause = $limitClause = '';
    
    if(isset($this->values['id'])) 
       $whereClause = ' WHERE id = :id' . (isset($this->values['timestamp']) ? ' AND ':' '); // id filter
       
    if(isset($this->values['timestamp'])) 
       ($whereClause = $this->values['id'] ? '':' WHERE ') . 'timestamp > :timestamp'; // timestamp filter


    $SQL = 'SELECT id, username, email, password, timestamp FROM ' . $this->tables['users'] . $whereClause . $limitClause; 
    
    if(isset($this->values['id'])): 
      $r = $this->injected['dbh']->prepare($SQL); 
      $r->bindValue(':id', $this->values['id'], PDO::PARAM_STR); 
      
      if(isset($this->values['timestamp'])) 
        $r->bindValue(':timestamp', $this->values['timestamp'], PDO::PARAM_INT);
        
      $d = $r->execute(); 
      if($d): 
        $res = $r->fetch(PDO::FETCH_ASSOC); 
        $r->closeCursor(); 
        return $res; // returns the fetched dataset alone
        
      else:  
        return 0; 
      endif; // something went wrong
      
    else:  
      $r = $this->injected['dbh']->prepare($SQL); 
          
      if(isset($this->values['timestamp'])) 
        $r->bindValue(':timestamp', $this->values['timestamp'], PDO::PARAM_INT);
        
      $d = $r->execute(); 
      
      if($d): 
        $res = $r->fetchAll(PDO::FETCH_ASSOC); 
        $r->closeCursor(); 
        return $res; // returns the fetched datasets
        
      else: 
        return 0; // something went wrong
      endif; 
    endif; 
  }
  
  // + + + + + + + + + + 
  
  
  // Is a helper function to get couples of ids => usernames
  
  public function getUsernames() {
    
    $whereClause = '';
    if(isset($this->values['id']))
      $whereClause .= ' WHERE id = ' . intval($this->values['id']);
      
    $r = $this->injected['dbh']->query('SELECT id, username FROM ' . $this->tables['users'] . $whereClause);
    if(!$r) return false;
    
    $res = isset($this->values['id']) ? $r->fetch(PDO::FETCH_ASSOC):$r->fetchAll(PDO::FETCH_ASSOC);
    
    if(!isset($this->values['id'])):
      $sortedByIds = [];
      foreach($res as $rec)
        $sortedByIds[$rec['id']] = $rec;
              
    else:
      $sortedByIds = [$res['id'] => $res];
    endif;
    
    $this->values = [];
    $r->closeCursor();
    
    return $sortedByIds;
  }

  // + + + + + + + + +




  public function delete() : bool {
    
    if(!$this->id): // invalid id parameter
       return false; 
      
    elseif($this->id == 1): // first registered account can't be deleted
       return false; 
      
    else:
      
       $r = $this->injected['dbh']->exec('DELETE FROM ' . $this->tables['users'] . ' WHERE id = ' . intval($this->values['id'])); 
       if(!$r) 
          return false;

       $r = $this->injected['dbh']->exec('DELETE FROM ' . $this->tables['users_profiles'] . ' WHERE id = ' . intval($this->values['id'])); 
       if(!$r)
          return false;
          
       return true; 
    endif;  
  }




  /**                                 LOGIN
   *
   *  @desc The login routine. It requires a matching username / email and password combinaison to create sessions and assume the user is logged in.
   *  @param String $idString Identifier string. The username or the e-mail adress related to the involved account.
   *  @param String $password A password as clear-text
   *  @return Integer Returns 1 if the login attempt succed or the value of Users::LOGIN_ERROR if the username, email or password is wrong. */
  
  public function login($idString, $password) {
  
    /* Determines either the end user is trying to log in using its username or using its e-mail adress to refer its account. */
    $whereClause = ' WHERE ';
    if(preg_match($this->regexp['email'], $idString)) 
      $whereClause .= ' email = :item '; // the user tries to log in using its e-mail adress
    else 
      $whereClause .= ' username = :item'; // the user tries to log in using its username


    $r = $this->injected['dbh']->prepare('SELECT id, username, email, password FROM ' . $this->tables['users'] . $whereClause); $r->bindValue(':item', $idString, PDO::PARAM_STR); $d = $r->execute();  // tries to find a user matching given identifier value
    
    if(!$d): 
      return 0; // something went wrong
    else: 
      $res = $r->fetch(PDO::FETCH_ASSOC); $r->closeCursor(); if(empty($res)) return self::LOGIN_ERROR; // if no record for this user
      
      if($this->password($password, $res['password'])): // password is correct, user is now logged-in
         $_SESSION['id'] = intval($res['id']); 
         $_SESSION['username'] = $res['username'];
         return 1; 
         
      else: 
         // password is incorrect
         return self::LOGIN_ERROR; 
      endif; 
    endif; 
    
  }




  /**  @desc Determines if the client is logged in as a registered user or not.
   *   @return Integer Returns 1 if the user is logged-in or not if he's not. */
   
  public function isLoggedIn() { 
    
    return isset($_SESSION['id']) && isset($_SESSION['username']) ? 1:0; 
    
  }




  /**                              LOG OUT                                    */
  
  public function logout() { 
    session_destroy(); 
  }
  
}
//                                   EOF
