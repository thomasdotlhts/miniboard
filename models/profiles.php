<?php 
namespace miniboard\Models;

use miniboard\includes\Models,
    \Exception, \PDO;
  
  
/* +-----------+--------------+------+-----+---------+----------------+
 * | Field     | Type         | Null | Key | Default | Extra          |
 * +-----------+--------------+------+-----+---------+----------------+
 * | id        | int(11)      | NO   | PRI | NULL    | auto_increment |
 * | user_id   | int(11)      | NO   |     | NULL    |                |
 * | picture   | varchar(255) | YES  |     | NULL    |                |
 * | location  | varchar(255) | YES  |     | NULL    |                |
 * | biography | text         | YES  |     | NULL    |                |
 * +-----------+--------------+------+-----+---------+----------------+       */
  
class ProfilesDbLayer extends Models {
  
  protected $tables = ['profiles' => 'users_profiles', 
                       'users' => 'users'];
  
  
  
  public function save() : bool {
    
    // Has a profil entry been saved or is it the first time the profile of the involved user is being edited?
    
    if(!isset($this->values['id']) && !isset($this->values['user_id']))
      return false;
      
    $whereClause = ' WHERE '; 
    if(isset($this->values['id']))
      $whereClause .= 'id = ' . intval($this->values['id']);
      
    if(isset($this->values['user_id']))
      $whereClause .= 'user_id = ' . intval($this->values['user_id']);
      
    $r = $this->injected['dbh']->query('SELECT COUNT(id) AS nbr FROM  ' . $this->tables['profiles'] . $whereClause);
        
    $nbr = (int) @$r->fetch(PDO::FETCH_ASSOC)['nbr'];
    $r->closeCursor();
    
    
    if(!$nbr):
      
      // Insert 
      
      $r = $this->injected['dbh']->prepare('INSERT INTO ' . $this->tables['profiles'] . ' VALUES (NULL, :user_id, :picture, :location, :biography)');
      
      $r->bindValue(':user_id', $this->values['user_id'], PDO::PARAM_INT);
      $r->bindValue(':picture', !empty($this->values['picture']) ? $this->values['picture']:null, PDO::PARAM_STR);
      $r->bindValue(':location', !empty($this->values['location']) ? $this->values['location']:null, PDO::PARAM_STR);
      $r->bindValue(':biography', !empty($this->values['biography']) ? $this->values['biography']:null, PDO::PARAM_STR);
      
      $d = $r->execute();
      return $d;
      
    else:
      
      // Update 
      
      $setArg = ' SET ';
      if(isset($this->values['picture']))
        $setArg .= ' picture = :picture';
      
      if(isset($this->values['location']))
        $setArg .= (!empty($setArg) ? ', ':'') . ' location = :location';
        
      if(isset($this->values['biography']))
        $setArg .= (!empty($setArg) ? ',':'') . ' biography = :biography';
      
      $r = $this->injected['dbh']->prepare('UPDATE ' . $this->tables['profiles'] . ' ' . $setArg . $whereClause);
      
      if(isset($this->values['picture']))
        $r->bindValue(':picture', $this->values['picture'], PDO::PARAM_STR);
      
      if(isset($this->values['location']))
        $r->bindValue(':location', $this->values['location'], PDO::PARAM_STR);
      
      if(isset($this->values['biography']))
        $r->bindValue(':biography', $this->values['biography'], PDO::PARAM_STR);
  
      
      $d = $r->execute();
      return $d;
    endif;
    
  }
  
  

  
  public function fetch() : array {
    
    $whereClause = '';
    
    if(isset($this->values['id']))
      $whereClause = sprintf(' WHERE %s.id = :id', $this->tables['profiles']);
    
    // Possible profiles.user_id filter 
    elseif(isset($this->values['user_id']))
      $whereClause = sprintf(' WHERE %s.id = :user_id', $this->tables['users']);
    

    // Request 

    $r = $this->injected['dbh']->prepare(sprintf('SELECT %1$s.username, %1$s.id AS user_id, %1$s.timestamp, %2$s.id, %2$s.picture, %2$s.location, %2$s.biography ' 
                                              .  'FROM %1$s LEFT JOIN %2$s ON %2$s.user_id = %1$s.id%3$s', $this->tables['users'], $this->tables['profiles'], $whereClause));

    if(isset($this->values['id']))
      $r->bindValue(':id', $this->values['id']);
      
    elseif(isset($this->values['user_id']))
      $r->bindValue(':user_id', $this->values['user_id']);
      
    $d = $r->execute();
    if(!$d) return [];
    
    $res = (isset($this->values['id']) || isset($this->values['user_id'])) ? $r->fetch(PDO::FETCH_ASSOC):$r->fetchAll(PDO::FETCH_ASSOC);
    $this->values = [];
    $r->closeCursor(); 
    
    return !$res ? []:$res;
    
  }
  
  
}
// EOF