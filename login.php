<?php 
namespace miniboard\Views;

use miniboard\includes\Views,
    miniboard\Models\Users;

/** The Login View to deal with users Login attempts. 
  * 
  * @author  : Thomas LHTS <thomas.lhts[at]protonmail.ch>
  * @date    : 2019.07
  * @license : https://opensource.org/licenses/MIT
  * @version : 0.1a */ 
class LoginView extends Views {

    // Logic
    public function onDisplay() {
      
      extract($this->injected);
    
      // Process a login attempt 
      if(!empty($_POST['login']) && is_array($_POST['login'])):

       $this->setVar('loggedInSuccess', $Users->login($_POST['login']['username'], $_POST['login']['password']));

      // + + + + + 

      
      // Possible redirection on success 
                              
      if($this->vars['loggedInSuccess'] == 1 && isset($_GET['referer'])):
         
        // Stop any useless process if a hackermind is trying to trick with the referer string 
        if(preg_match('/http:\/\//', $_GET['referer']) || preg_match('/www\./', $_GET['referer'])):
          echo 'Cross-domain redirection attempt blocked.';
          exit();
        endif;

        // Wanna avoid a header() injection here
        $needle = preg_replace("/[^A-Za-z0-9\/]/", '', $_GET['referer']);

        // Is this only a single module argument, or is this a potential [admin/]module/[xxx]/ pattern ?
        $needle = preg_match('#\/#', $needle) ? explode('/', $needle):$needle;

        // take care of a referer string with multiple params separated by a '/'
        if(is_array($needle)): 

          $admin = $needle[0] == 'admin' ? 'admin/':''; // is the redirection related to an admin module's view?
       
          if(!empty($admin)):
            //  To an administration page redirection 
            if(count($needle) < 2 && count($needle) > 3): // as it should be admin/xyz/[xyz/] with a mandatory module name and an optional action param.
              echo 'Invalid redirection attempt.';
              exit();
            endif;
            $module = isset($needle[1]) ? $needle[1] . '/':'';
            $action = !empty($needle[2]) ? $needle[2] . '/':'';
      
          else:
      
            // To a standard page redirection
            $module = isset($needle[0]) ? $needle[0] . '/':'';
            $action = !empty($needle[1]) ? $needle[1] . '/':'';
          endif;
      
         else:
           $admin = $action = '';
           $module = $needle;
         endif;

         if(empty($module)):  // ..is the only variable that must not to be empty
           echo 'Invalid redirection attempt.'; exit();
         endif;

         // Now we're able to redirect the end-user by the way of a sanitized URI.
         $toRedirectURI =  BASE_URI . $admin . $module . $action;
         header('Refresh:5; url=' . $toRedirectURI);


       elseif($this->vars['loggedInSuccess'] == 1):
         
         // No referer pattern given, home redirection 
         header('Refresh:3 ' . BASE_URI);  
         
       endif;
      endif;
      
      // + + + + + 
      
      
      $this->setFile('header.tpl.php');
      $this->setFile('login.tpl.php');
      
    }  
  
}
// EOF