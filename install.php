<?php 
use miniboard\includes\Models,
    miniboard\Models\ConfigDbLayer,
    miniboard\Models\Users;

require 'includes/functions.php';
require 'includes/models.php';
require 'models/users.php';
require 'models/config.php';


if(!empty($_POST)): 



  // Setup project 
  
  if(!empty($_POST['project']['uri'])):
    
    $projectURI = trim($_POST['project']['uri']);
    $projectURI = $projectURI[strlen($projectURI) - 1] == '/' ? $projectURI:$projectURI . '/';  
  else: 
    
    echo 'Missing project URI.';
    exit();
      
  endif;




  // Setup database 

  if(!empty($_POST['database'])):
     [$dbHost, $dbUsername, $dbPassword, $dbName, $prefix] = [trim($_POST['database']['host']), trim($_POST['database']['username']), 
                                                              trim($_POST['database']['password']), trim($_POST['database']['dbname']), 
                                                              trim($_POST['database']['prefix'])];

     // ensure last character of the prefix (if a prefix has been specified) is an underscore
     if(!empty($prefix))
       $prefix = $prefix[strlen($prefix) - 1] == '_' ? $prefix:$prefix . '_'; 
    
     // Checking values
     switch(true):
       case (empty($dbHost)):
          echo 'Missing database host.'; 
          exit();
       break;
       case (empty($dbUsername)):
          echo 'Missing database username.';
          exit();
       break;
       break;
       case (empty($dbName)):
          echo 'Missing database name.';
          exit();
       break;
     endswitch;
     
     
     // Trying to reach MySQL through PDO 
     $dbh = __db($dbHost, $dbUsername, $dbPassword, $dbName);
     

     if(!$dbh):
       echo 'Failed to connect to MySQL. Please double check given informations and try again.';
       exit();
     endif;
     
     // Create tables 
     $SQL = preg_replace('/\{\{prefix\}\}/', (!empty($prefix) ? $prefix:''), file_get_contents('dump.sql'));
     $d = $dbh->exec($SQL);

  else:
    echo 'Missing database informations.';
    exit();
  endif;
  
  
  
  // First account 
  
  if(!empty($_POST['user'])):
    
      // Create first account 
      $Users = new Users();
      $Users->inject('dbh', $dbh);
      
      if(isset($_POST['database']['prefix']) && !empty(trim($_POST['database']['prefix'])))
         $Users->setTablesPrefix(trim($_POST['database']['prefix']));
      
         $userCreated = $Users->set('username', $_POST['user']['username'])
                              ->set('password', $_POST['user']['password'])
                              ->set('passwordConfirmation', $_POST['user']['password'])
                              ->set('mail', $_POST['user']['email'])
                              ->save();
        
        
        if(!$userCreated):
        
          echo 'Failed to create administrator user account, please try again.';
          exit();
        
        else:
          
          // Generate configuration 
          $generated = '<textarea rows="10">'  . PHP_EOL
                      . '<?php' . PHP_EOL
                      . 'define(\'BASE_URI\', \'' . $projectURI . '\');' . PHP_EOL
                      . 'define(\'CONFIG\', [' . PHP_EOL
                      . "\t\t\t" . '\'host\'    ' . "\t\t" . '=>' . "\t\t" . '\'' . $dbHost . '\','      . PHP_EOL
                      . "\t\t\t" . '\'username\'' . "\t\t" . '=>' . "\t\t" . '\'' . $dbUsername . '\','  . PHP_EOL
                      . "\t\t\t" . '\'password\'' . "\t\t" . '=>' . "\t\t" . '\'' . $dbPassword . '\','  . PHP_EOL
                      . "\t\t\t" . '\'dbname\'  ' . "\t\t" . '=>' . "\t\t" . '\'' . $dbName     . '\','  . PHP_EOL
                      . "\t\t\t" . '\'prefix\'  ' . "\t\t" . '=>' . "\t\t" . '\'' . $prefix . '\''       . PHP_EOL
                      . '                 ]);' . PHP_EOL
                      . '// EOF' . PHP_EOL
                     . '</textarea>';
                     
        endif; 
                
  else:
    echo 'Missing account informations.';
    exit();
  endif;

endif; 
?>



<!DOCTYPE html>
<html>
 <head>
   <title>Miniboard installation</title>
   <link href="static/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
   <meta charset="utf-8" />
   <style>
     .bold { font-weight: bold; }
     textarea { width: 100%; font-family: monospace; }
   </style>
 </head>
 <body>
   
   <div class="container-fluid">
     <div class="row">
       <div class="col-xl-6 col-lg-6 col-md-8 col-12 px-5 py-5">
       
         <h1>Miniboard</h1>
         
         <?php 
         if(!isset($userCreated)): ?>
            <p class="text-secondary">
               This tool guides you to deploy a new copy of the Miniboard light-weight discussions program.
            </p>
       
            <form method="POST">
              
              
              <!-- Project's configuration -->

              <div class="row">
                <div class="col-12">
                  <p class="bold mt-4">Configure project</p>
                </div> <!-- /.col -->
              </div> <!-- /.row -->

              <div class="row">
                 <div class="col-6">
                   <p>
                     Project URI : 
                     <input type="text" name="project[uri]" class="form-control" placeholder="Project's root folder" />
                   </p>
                 </div>
              </div> <!-- /.row -->
              


              <!-- Database configuration -->
              
              <div class="row">
                <div class="col-12">
                  <p class="bold mt-4">Configure database</p>
                </div>
              </div> <!-- /.row -->
           
              <div class="row">
                 <div class="col-6">
                   <p>
                     MySQL host : 
                     <input type="text" name="database[host]" class="form-control" placeholder="MySQL server hostname" />
                   </p>        
                 </div> <!-- /.col -->
                 <div class="col-6">
                 <p>
                   MySQL username : 
                   <input type="text" name="database[username]" class="form-control" placeholder="Account user" />
                 </p>
               </div> <!-- /.col -->
              </div> <!-- /.row -->
           
             <div class="row">
               <div class="col-6">
                 <p>
                   MySQL password : 
                   <input type="password" name="database[password]" class="form-control" placeholder="Account password" />
                 </p>
               </div> <!-- /.col-6 -->
               <div class="col-6">
                 <p>
                   MySQL database name : 
                   <input type="text" name="database[dbname]" class="form-control" placeholder="Database name" />
                 </p>
               </div> <!-- /.col-6 -->
             </div> <!-- ./row -->
           
             <div class="row">
               <div class="col-6">
                  <p>
                    Tables prefix (optional): 
                    <input type="text" name="database[prefix]" class="form-control" placeholder="Table prefix" />
                  </p>
               </div> <!-- /.col -->
             </div> <!-- /.row -->
                
             
           
       
             <!-- First account configuration -->
             
             <div class="row">
               <div class="col-12 pt-5">
                 <p class="bold">Configure your account</p>
               </div>
             </div> <!-- /.row -->
           
             <div class="row">
               <div class="col-6">
                  <p>
                    Username : 
                    <input type="text" name="user[username]" class="form-control" placeholder="Your username"/>
                  </p>
               </div> <!-- /.col -->
               <div class="col-6">
                  <p>
                    Email address : 
                    <input type="text" name="user[email]" class="form-control" placeholder="Your username"/>
                  </p>
               </div> <!-- /.col -->
             </div> <!-- /.row -->
    
             <div class="row">
              <div class="col-6">
                <p>
                  Password : 
                  <input type="text" name="user[password]" class="form-control" placeholder="Your password" />
                </p>
              </div> <!-- /.col -->
             </div> <!-- /.row -->
    
             <div class="row">
               <div class="col-12">
                  <button class="btn btn-primary mt-5">Process</button>
               </div> <!-- /.col -->
             </div> <!-- /.row -->
          </form>
         
        <?php 
        else: ?>
        
          <div class="row">
            <div class="col-12 py-4">
              <p>
                <span class="bold">Everything ran well!</span>
                 You can now write this configuration sample to the <code>config.php</code> file.
              </p>
              
              <p>
                <!-- Display generated configuration -->
                <?php echo $generated ?>
              </p>
              
              <p>
                <a href="<?= $projectURI ?>" class="btn btn-primary">Get started</a>
              </p>
              
            </div> <!-- /.col -->
          </div> <!-- /.row -->
        
        <?php 
        endif; ?>  
       </div> <!-- /.col -->
     </div> <!-- /.row -->
   </div>
   
 </body>
</html>
<?php // EOF ?>