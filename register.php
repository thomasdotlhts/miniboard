<?php 
namespace miniboard\Views;

use miniboard\includes\Views,
    miniboard\Models\Users;
    
class RegisterView extends Views {
  
  public function onDisplay() {
    
    // New users registration process 
    
    if(!empty($_POST['register']) && is_array($_POST['register'])):
      // Toggle comments to toggle new users registration.
      // echo 'Registrations are disabled.';
      // exit();
      
      if($this->injected['Users']->isLoggedIn()):
        header('Location: ' . BASE_URI);
        exit();
      endif;
      
      $this->injected['Users']->set('username', $_POST['register']['username']);
      $this->injected['Users']->set('password', $_POST['register']['password']);
      $this->injected['Users']->set('passwordConfirmation', $_POST['register']['passwordConfirmation']);
      $this->injected['Users']->set('mail', $_POST['register']['email']);
      
      $this->setVar('userRegisteredSuccess', $this->injected['Users']->save());
      
    endif;
    
    $this->setFile('header.tpl.php');
    $this->setFile('register.tpl.php');
  }
  
}

// EOF