<?php 
namespace miniboard\Views;

use miniboard\includes\Views, 
    miniboard\Models\ThreadsDbLayer,
    miniboard\Models\PostsDbLayer;


/** The Thread View to deal with the Threads logic. 
  * @author : Thomas LHTS <thomas.lhts[at]protonmail.ch>
  * @date: 2019.07
  * @license : https://opensource.org/licenses/MIT
  * @version: 0.1a */
  
class ThreadView extends Views {


    public function onDisplay() : void {
      
      // ThreadsDbLayer Model
      $Threads = new ThreadsDbLayer();
      $Threads->inject('dbh', $this->injected['dbh']);
      
      // PostsDbLayer Model 
      $Posts = new PostsDbLayer();
      $Posts->inject('dbh', $this->injected['dbh']);
            
      // + + + + + 


      $isLoggedIn = $this->injected['Users']->isLoggedIn();
      $isAdministrator = $this->injected['Config']->isAdministrator();  
      $isModerator = $this->injected['Config']->isModerator();
      
      // Status shorthands to template 
      
      $this->setVar('isLoggedIn', $isLoggedIn);
      $this->setVar('isAdministrator', $isAdministrator);
      $this->setVar('isModerator', $isModerator);
      
      // + + + + +




      /* Update thread's datas
         + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + +  */   
      
      if(!empty($_POST)):
        
        // Update an existing thread 
        if(isset($_POST['thread']['id']) && isset($_POST['thread']['title']) 
            && isset($_POST['thread']['content']) && isset($_POST['thread']['first_post_id'])):
          
          // Update thread's metadatas 
          $threadUpdatedSuccess = $Threads->set('id', $_POST['thread']['id'])->set('title', $_POST['thread']['title'])->save();
                                          
          // Update threads' first post's content
          $postUpdatedSuccess = $Posts->set('id', $_POST['thread']['first_post_id'])->set('content', $_POST['thread']['content'])->save();
                      
          $this->setVar('threadUpdatedSuccess', ($threadUpdatedSuccess && $postUpdatedSuccess));                                                                    
        endif;
      endif;




      /* Get the selected thread record for edition 
         + + + + + + + + + + + + + + + + + + + + + + */

      if(isset($_GET['edit']) && isset($_GET['id'])):
        
        $selectedThread = $Threads->set('id', $_GET['id'])->fetch();
        
        if(!empty($selectedThread))
          $selectedThread['content'] = $this->injected['Purifier']->purify($selectedThread['content']);
          
        $this->setVar('selectedThread', $selectedThread);
          
        
      endif;
            
      
    
      /* Related to the selected thread's feed of posts 
         + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + +  */
      
      
      if(!isset($_GET['edit'])): // refers to the thread's edition mode. 
        if(isset($_GET['id'])): // ensure a thread id has been specified 
              
          // Save a post to the database     
          if(!empty($_POST['post']['content']) && $isLoggedIn): 
            
            // Double-check if the end user is actually able to write to this thread. 
            
            if(isset($_POST['post']['id'])) 
              // In case the end-user wants to edit a post, we need to ensure he do really owns the specified post. 
              $isOwned = $Posts->set('id', $_POST['post']['id'])->isOwnPost();
                      
            else
              $isOwned = 1; // adding a new post to the thread  
              
              
            // + + 


            // Prevent new posts to be appended to a locked thread  
                          
            $isThreadLocked = $Threads->set('id', $_GET['id'])->isLocked();

            if((!$isOwned || $isThreadLocked) && (!$isModerator && !$isAdministrator)) // may happen only if a evil mind manually crafts the value of $_POST['post']['id']
               _exit();


            // + + 
             
            
            if(isset($_POST['post']['id']))
               $Posts->set('id', $_POST['post']['id']);

            $this->setVar('postUpdatedSuccess', $Posts->set('thread_id', $_GET['id'])
                                                      ->set('content', $_POST['post']['content'])
                                                      ->set('owner', $_SESSION['id'])
                                                      ->save());      
          endif; 
          
            
          // + +
        
          
          // The user wants to edit a post so provide him the related data. 
          if(isset($_GET['post'])):
            
            $selectedPost = $Posts->set('id', $_GET['post'])->fetch();
            $selectedPost['content'] = $this->injected['Purifier']->purify($selectedPost['content']);
            
            $this->setVar('selectedPost', $selectedPost);
            
          endif;
                                                        
        else:
          exit(); // no thread id specified 
        endif;
      endif;




      /* Process moderation mode actions
         + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + +  */
         
      if(isset($_POST['moderate'])):
        
        $mode = null;
        $foundIds = [];
        
        foreach($_POST['moderate'] as $k => $v):
          if(is_numeric($k)):
            $foundIds[] = intval($k);
            
          else:
            switch($k):
              case 'delete':
                $mode = $k;
              break;
            endswitch;  
          endif;
        endforeach;
        
        // Process the necessary action on the given ids 
        
        switch($k):
          case 'delete':

            // Delete selected posts 
            $Posts->set('id', $foundIds);
            $Posts->delete();
          break;
        endswitch;
      endif;
      



      /* From user-mode single post deletion 
         + + + + + + + + + + + + + + + + + + */
         
         
      // Notice a simple user can't delete several posts at once 
         
      if(!empty($_POST['delete']) && !empty($_POST['delete']['post']) && !empty($_POST['delete']['post']['id'])): 
        
        // Check if its own post. Notice administrators & moderators are able to delete all posts.
        if($Posts->set('id', intval($_POST['delete']['post']['id']))->isOwnPost() || $isModerator || $isAdministrator): 
          
          // Ensure we are not deleting the thread's starting post.
          if(!$Posts->set('id', intval($_POST['delete']['post']['id']))->isRemovable())
             _exit();
          
          
          // Delete the specified post 
          $Posts->set('id', intval($_POST['delete']['post']['id']))
                ->delete();
          
          
        else: // only a evil mind would try to delete someone else post :) 
          _exit();
        endif;
                
      endif; 


  
      /* Get the selected thread related posts 
         + + + + + + + + + + + + + + + + + + + */      
      
      $Threads->set('id', $_GET['id']);
      $Threads->setCurrentPage($_GET['page']);
      $Threads->setLimitPerPage(10);
      
      
      // Get post lists and parse their content using Parsedown
      $postsList = $Threads->getPosts();


      // Markdown is parsed then content is made XSS-proof
      foreach($postsList as $k => $post)
          $postsList[$k]['content'] = $this->injected['Purifier']->purify($this->injected['Parsedown']->parse($post['content']));


      $this->setVar('postsList', $postsList)
      
           ->setVar('paginationComponentDOM', self::_component_pagination($Threads->getCurrentPage(), $Threads->getNumberOfPages(), sprintf('%sthread/%d/p/', BASE_URI, (int) $_GET['id'])))
           
           ->setFile('header.tpl.php')
           ->setFile('brand.tpl.php')
           ->setFile('thread.tpl.php')
           ->setFile('footer.tpl.php');
    }

}
// EOF 
?>