<?php 
/** The Miniboard program entry point.
  *
  * @author  : Thomas LHTS <thomas.lhts[at]protonmail.ch>
  * @date    : 2019.07
  * @license : https://opensource.org/licenses/MIT
  * @version : 0.1a */

use miniboard\includes\Models, 
    miniboard\includes\Views,

    miniboard\Views\HomeView,
    miniboard\Views\ThreadView,
    miniboard\Views\ProfileView,
    miniboard\Views\LoginView,
    miniboard\Views\RegisterView,
    miniboard\Views\ConfigView,

    miniboard\Models\Users,
    miniboard\Models\ConfigDbLayer;


require 'config.php';

require 'includes/functions.php';
require 'includes/models.php';
require 'includes/views.php';

require 'models/config.php';
require 'models/threads.php';
require 'models/posts.php';
require 'models/profiles.php';
require 'models/users.php';

require 'admin/config.php';

require 'third/parsedown.php';
require 'third/htmlpurifier/HTMLPurifier.auto.php';

require 'home.php';
require 'thread.php';
require 'login.php';
require 'register.php';
require 'profile.php';


if(!defined('CONFIG')):
  echo '<p>This copy of the Miniboard program hasn\'t been configured yet.<br />'
     . 'Please run the <a href="install.php">installer</a>.</p>';
  exit();
endif;

session_start();


// Temp logout procedure
if(isset($_GET['logout'])):
  session_destroy();
  header('Location: ' . BASE_URI);
  exit();
endif;


// Connect to MySQL
$dbh = __db(CONFIG['host'], CONFIG['username'], CONFIG['password'], CONFIG['dbname']);


// Users model instance 
$Users = new Users();
$Users->inject('dbh', $dbh);


// Config model to deal with the platform's configuration 
$Config = new ConfigDbLayer();
$Config->inject('dbh', $dbh);


// Parsedown instance 
$Parsedown = new Parsedown();
$Parsedown->setMarkupEscaped(false)
          ->setBreaksEnabled(true);


// HTMLPurifier instanciation 
$PurifierConfig = HTMLPurifier_Config::createDefault();
$PurifierConfig->set('HTML.AllowedElements', 'p, a, em, strong, ul, ol, li, h1, h2, h3, h4, h5, h6, table, thead, tbody, tr, td, code, pre');
$PurifierConfig->set('HTML.AllowedAttributes', '');
$PurifierConfig->set('AutoFormat.RemoveEmpty', true);
$Purifier = new HTMLPurifier($PurifierConfig);


// Take care of miniboard's default theme 
Views::themes($Config->get('theme'));


// + + + + + + + + + + + 




// Determine which view is asked 

switch($_GET['view']):
  
  
  // The Home View 
  case 'home':
  
    $HomeView = new HomeView();
    $HomeView->inject('dbh', $dbh)
             ->inject('Users', $Users)
             ->inject('Config', $Config)
             ->display();
  break;
  
  
  // The Login View 
  case 'login':
  
    $LoginView = new LoginView();
    $LoginView->inject('dbh', $dbh)
              ->inject('Users', $Users)
              ->inject('Config', $Config)
              ->display();
  break; 


  // The Register View 
  case 'register':
  
    $RegisterView = new RegisterView();
    $RegisterView->inject('dbh', $dbh)
                 ->inject('Users', $Users)
                 ->inject('Config', $Config)
                 ->display();
  break;


  // The Profile View
  case 'profile':
  
    $ProfileView = new ProfileView();
    $ProfileView->inject('Users', $Users)
                ->inject('Config', $Config)
                ->inject('dbh', $dbh)
                ->display();
  break;
  
  
  // The Thread View 
  case 'thread':
  
    $ThreadView = new ThreadView(); 
    $ThreadView->inject('dbh', $dbh)
               ->inject('Users', $Users)
               ->inject('Config', $Config)
               ->inject('Parsedown', $Parsedown)
               ->inject('Purifier', $Purifier)
               ->display();
  break;
  
  
  // The Config View
  case 'config': 
  
    $ConfigView = new ConfigView();
    $ConfigView->inject('dbh', $dbh)
               ->inject('Purifier', $Purifier)
               ->inject('Users', $Users)
               ->display();
  break;
  
  // Default 
  default:
     echo 'Invalid request.';
     exit();
  break;
  
endswitch;
// EOF