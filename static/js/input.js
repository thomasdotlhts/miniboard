/**
  *  Basic upload script (frontend)
  * 
  *  The purpose of this file is to provides an object to make asynchronous 
  *  files uploads easier. It listens to the changes on the specified 
  *  input[type="file"] and read selected image files using the 
  *  FileReader API. An built-in function is provided to sent the files as 
  *  blobs to the server. The server script is also provided with these files.
  *
  *  @author  : Thomas LHTS <thomas.lhts[at]protonmail.ch>
  *  @date    : 2019.14.07 
  *  @version : 0.1a                                                          */
     
let InputFiles = {
  
  // This tracks all files selected with the dedicated input file      
  selectedFiles: [],
  
  // The API URL to send blobs to  
  apiURL: undefined,
  
  
  // Custom directory path to upload files to. 
  customDirectory:undefined,
  
  
  // Type of upload 
  type:undefined,
  
  
  // Tracks the number of selected files 
  filesCounter: 0,
  
  
  // Classes of elements built dynamically are kept here for maintainability concerns
  classes:{
    
    thumbnailContainer:'thumbnail-container',    
    deletionLink:'thumb-deletion-link',
    loader:'loader-gif',
    field:'images-picker-group'
    
  },
  
  // Elements css selectors provided by the end-user 
  selectors: {
    
    inputSelector:null, 
    thumbsContainerSelector:null,
    linksContainerSelector:null,
    triggerSelector: null
    
  },
  
  // + + + + + + + + + + 
  
  
  
  
  /** Takes the input[type="file"] used to upload files selector, and the container 
     you want to append <img /> thumbails' tag into. Params are a `args` object used
     to conveniently give some additional configuration params like :
     
     - args.inputSelector :              Selector of the involved input[type=file] element.
     - args.thumbsContainerSelector :    Selector of the container to append thumbnails to.
     - args.linksContainerSelector :     Selector of the uploaded images link container
     - args.triggerSelector :            Selector of the element which would trigger the send-blobs request.
     - args.url :                        URL to send blob to.
     
     
     - args.urlPrefix :                  The string value to be used a URL prefix for the generated links (optional). (@TODO: Fix ambiguous name)
     
     
     - args.hideControls :               Specify if the input selector needs to be hid as upload is success
     
     
     - args.customDirectory :            A custom directory specifed to upload the files. Notice the directory 
                                         should belong to a list of allowed directories. See the /upload.php file for 
                                         more informations. 
                                         
     - args.type :                       The (optional) " type " of upload may be specified to trigger the correct 
                                         callback function.
                
                
     - args.imageContainer :             This (optional) parameter gives the ability to specify a image container (that may be a div, p or img node)
                                         to append the uploaded-with success image. This parameter is useful in the case of a single-image upload 
                                         like when it comes, for example, to upload a profile image. 
                                         
     - args.imageURIPrefix               This (optional) parameter is used to specify a prefix for the returned by the server uploaded images URI. 
     
     - args.maxNbOfFiles                 To specify the number of file uploadable at once. Default is unlimited.
                                         
                                                                              */
  
  init: function(args) {
    
    if(!args.inputSelector)            return false
    if(!args.thumbsContainerSelector)  return false 
    if(!args.triggerSelector)          return false 
    if(!args.url)                      return false
    
    this.selectors.inputSelector = args.inputSelector
    this.selectors.thumbsContainerSelector = args.thumbsContainerSelector
    this.selectors.linksContainerSelector = args.linksContainerSelector
    this.selectors.triggerSelector = args.triggerSelector
    
    this.apiURL = args.url
    this.urlPrefix = args.urlPrefix
    
    this.hideControls = args.hideControls;
    this.customDirectory = args.customDirectory
    this.type = args.type
    this.imageContainer = args.imageContainer
  
    this.imageURIPrefix = args.imageURIPrefix
    
    this.maxNbOfFiles = args.maxNbOfFiles
  
    // Reference to the element that would trigger the upload process
    this.uploadTrigger = document.querySelector(args.triggerSelector)
  
    // Reference to the input field itself.
    let input = document.querySelector(args.inputSelector) 
     
    // Container we'll append the thumnbnails images & links into later
    let previewer = document.querySelector(args.thumbsContainerSelector)
    
    if(args.linksContainerSelector != undefined) 
      var links = document.querySelector(args.linksContainerSelector)
    
    
    // As some files are selected 
    input.addEventListener('change', function() {
    
      if(InputFiles.maxNbOfFiles && InputFiles.selectedFiles.length >= InputFiles.maxNbOfFiles) {
        alert("You can only pick " + InputFiles.maxNbOfFiles + " file" + (InputFiles.maxNbOfFiles > 1 ? 's':'') + " at once." ) 
        return false;
      }
      
      if(input.files.length > 0) {
        
        // Loop over input's FilesList
        for(var i = 0, file; file = input.files[i]; i++) {

          // Invalid mime types are skipped 
          if(file.type != 'image/png' && file.type != 'image/jpeg' && file.type != 'image/gif') {
            alert('An invalid file has been skipped.')
            continue
          }
    
          // Read file 
          let reader = new FileReader(); 
          reader.onloadend = InputFiles.fileLoadedEv
          reader.readAsDataURL(file)        
        }
      }  
    });
    
    
    // As upload button is clicked
    this.uploadTrigger.addEventListener('click', function() { 
      
      document.querySelector(`.${InputFiles.classes.field}`).classList.add('d-none') // hide the selection field 
      document.querySelector(`.${InputFiles.classes.loader}`).classList.remove('d-none') // show the loader 
      document.querySelector(args.thumbsContainerSelector).innerHTML = ''; // reset the preview area 

      // Start the upload process 
      InputFiles.upload()
    })
    
  },




  // Asynchronous file upload function 

  upload:function() {
    
    if(!this.selectedFiles.length) return false // no file selected
    
    InputFiles.uploadTrigger.classList.add('d-none'); // hide the upload button

    let xhr = new XMLHttpRequest()
    xhr.onreadystatechange = function() {

      if(this.readyState === XMLHttpRequest.DONE && this.status == 200) {

        document.querySelector(`.${InputFiles.classes.loader}`).classList.add('d-none') // hide the loader gif 

        let response = xhr.response    
              
        switch(InputFiles.type) {
          
          case 'single-image':
          case 'avatar':
            if(InputFiles.imageContainer) {

               let imageContainer = document.querySelector(InputFiles.imageContainer)

               if(imageContainer) {
                 
                 let src = (this.imageURIPrefix ? this.imageURIPrefix:'') + response[0]
                 
                 if(imageContainer.tagName == 'IMG') {
                   imageContainer.setAttribute('src', )
                 }
               
                 else {
                   
                   let possibleImgTag = imageContainer.querySelector('img')

                   if(!possibleImgTag) {
                     let imgElement = document.createElement('img')
                   
                     /** @WARNING @TODO : Endure this is not a distant file but a file that is stored on the local server. */
                     imgElement.setAttribute('src', response[0])
                     imageContainer.append(imgElement)
                   }
                   
                   else {
                     possibleImgTag.setAttribute('src', response[0])
                   }
                 }
               }  
            }
          break;
          
          default:
            // Display the list of links of all uploaded images          
            if(InputFiles.selectors.linksContainerSelector) {
              
              let ulContainer = document.createElement('ul')
              for(key in xhr.response) {
              
                let liElem = document.createElement('li')
                let inputElement = document.createElement('input')
              
                // Link to uploaded image
                inputElement.value = InputFiles.urlPrefix + '/' + xhr.response[key]
              
                liElem.append(inputElement)
                ulContainer.append(liElem) // add images links to DOM
                
                let links = document.querySelector(InputFiles.selectors.linksContainerSelector)
                links.append(ulContainer) // append the list of links to the DOM
             }          
           break;
         }
       }
       
       
       if(!InputFiles.hideControls) {
          document.querySelector(`.${InputFiles.classes.field}`).classList.remove('d-none') // show the selection field back 
          InputFiles.uploadTrigger.classList.remove('d-none'); // show the upload button back
       }
       
     }
   }
    
   xhr.open('POST',  this.apiURL)
   xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded')
   xhr.responseType = 'json';

   var reqBody = {'directory': InputFiles.customDirectory, 
                  'blobs': InputFiles.selectedFiles}
                  
   if(InputFiles.type) reqBody.type = InputFiles.type
   reqBody.type = InputFiles.type
    
   xhr.send(JSON.stringify(reqBody))
   this.selectedFiles = [];
  },

  // + + + + + + + + + + 




  // Events related functions 
  
  
  // As a file has been loaded by the client 
  fileLoadedEv:function(e) {
    
    // Image as base64
    let blob = e.target.result 
    InputFiles.selectedFiles.push(blob);
    
    // Thumbnail image container 
    let imgContainer = document.createElement('div')
    imgContainer.classList.add(InputFiles.classes.thumbnailContainer)
    imgContainer.setAttribute('data-index', InputFiles.filesCounter)
    
    // Delete before upload button 
    let deleteLink = document.createElement('a')
    deleteLink.classList.add(InputFiles.classes.deletionLink)
    deleteLink.href = '#'
    deleteLink.innerHTML = "Remove"
              
    // Delete thumb as clicked 
    deleteLink.addEventListener('click', InputFiles.deleteThumbEv)
    
    // Thumbnail image element 
    let img = document.createElement('img')
    img.setAttribute('src', blob)
    
    // Add the thumbnail element to the previewer zone
    imgContainer.append(img, deleteLink)
    
    let previewer = document.querySelector(InputFiles.selectors.thumbsContainerSelector) 
    previewer.append(imgContainer)

    if(previewer.classList.contains('d-none')) 
        previewer.classList.remove('d-none') 

    InputFiles.filesCounter++;
  },
  
  
  
  
  // Delete a thumb in the list of thumbs ev. 
  
  deleteThumbEv:function(e) {
    e.preventDefault()
    
    // Reach the thumbnail container of the clicked button 
    let container = e.target.closest(`.${InputFiles.classes.thumbnailContainer}`)
    let index = container.getAttribute('data-index')               
    InputFiles.selectedFiles[index] = null;  
    container.remove()
  }  
  
}
// + + + + + + + + + + 

// EOF