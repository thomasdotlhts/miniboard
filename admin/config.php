<?php 
namespace miniboard\Views;
use miniboard\includes\Views,
    miniboard\Models\ConfigDbLayer;
    
class ConfigView extends Views {
  
  
  // Logic callback 
  public function onDisplay() {
        
    $Config = new ConfigDbLayer();
    $Config->inject('dbh', $this->injected['dbh']);
    
    // Access reserved to administrators users only 
    if(!$Config->isAdministrator()):
      header('Location: ' . BASE_URI);
      exit();
    endif;


      
      
    /* Set metadatas 
       + + + + + + + */
    
    if(!empty($_POST['metadatas'])):
      
      $Config->set('title', $_POST['metadatas']['title']);
      
    endif;




    /* Attribute rights 
       + + + + + + + + + */
    
    if(!empty($_POST['rights']) && isset($_POST['rights']['user_id']) && isset($_POST['rights']['right'])):
    
        switch(intval($_POST['rights']['right'])):
          
          // Set moderator 
          case 0:
            $Config->isModerator((int) $_POST['rights']['user_id'], $setter = 1);
          break;
          
          // Set administrator 
          case 1:
            $Config->isAdministrator((int) $_POST['rights']['user_id'], $setter = 1);
          break;

        endswitch;
     endif;

    
     // Right revocations 
     
     if(!empty($_POST['revoke']) && isset($_POST['revoke']['right']) && isset($_POST['revoke']['user_id'])):
       
       switch($_POST['revoke']['right']):
         case 'adm':
          $Config->isAdministrator((int) $_POST['revoke']['user_id'], $setter = 1, $drop = 1);
         break;
         
         case 'mod':
          $Config->isModerator((int) $_POST['revoke']['user_id'], $setter = 1, $drop = 1);
         break;
         
       endswitch;
     endif;
     
     
     
     /*
        Theme picker 
        + + + + + + +  */
        
     if(!empty($_POST['theme']) && isset($_POST['theme']['custom'])):
       $Config->set('theme', $_POST['theme']['custom']);
       $this->setVar('themeUpdatedSuccess', true);
  
     endif;
     
     
     // + + + + + + + + + + 
    
    
    

     // Setup template 
    
     $this->setVar('title', $Config->get('title'))
          ->setVar('moderatorsList', json_decode($Config->get('moderators'), true))
          ->setVar('administratorsList', json_decode($Config->get('administrators'), true))
          ->setVar('usersList', $this->injected['Users']->fetch())
          ->setVar('usernamesIdsCouples', $this->injected['Users']->getUsernames())
          ->setVar('themesList', Views::themes())
          
          ->setFile('header.tpl.php')
          ->setFile('brand.tpl.php')
          ->setFile('config.tpl.php');
  }
}
// EOF
