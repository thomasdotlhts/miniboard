<?php 
use miniboard\includes\Models,
    miniboard\Models\ImageUpload,
    miniboard\Models\ProfilesDbLayer;

require 'config.php';
require 'includes/functions.php';
require 'includes/models.php';
require 'models/profiles.php';
require 'models/uploads.php';

session_start();


// Connect to MySQL
$dbh = __db(CONFIG['host'], CONFIG['username'], CONFIG['password'], CONFIG['dbname']);


// Anonymous users can't upload files to the server. 
if(!isset($_SESSION['id']))
  exit();


ini_set("memory_limit","256M");
ini_set('gd.jpeg_ignore_warning', 1);

$allowedDirectories = ['static/uploads/avatars/'];

$reqBody = json_decode(file_get_contents('php://input'), true); // request's body contains our images data as stringyfied binary datas

if(!in_array($reqBody['directory'], $allowedDirectories))
   exit(); 
  
$uploadedImagesPaths = [];  
$ImageUpload = new ImageUpload($reqBody['directory']);




// Additional configuration for each special cases 

switch($reqBody['type']):
  
  case 'avatar':
    $ImageUpload->set('width', 100);
  break;
  
endswitch;


// From blobs to files

foreach($reqBody['blobs'] as $blob)
  if($blob != NULL) // as some added then deleted files may appear as NULL
    $uploadedImagesPaths[] = $ImageUpload->upload($blob);
    
    
if(empty($uploadedImagesPaths))
  exit();




/** The $reqBody['type'] specify the type of upload it is to define and make sure 
  * the correct callback function would be called after the file has been uploaded. 
  * If no type has been specified, then no callback function will be called on 
  * success. */ 

if(isset($reqBody['type']) && is_string($reqBody['type'])):
  
  // Callback function when it comes to upload an avatar.
  switch($reqBody['type']):
    
    case 'avatar':

      // @TODO : Allow administrators to edit picture images from profile. 
      
      $ProfilesDbLayer = new ProfilesDbLayer(); 
      $ProfilesDbLayer->inject('dbh', $dbh);

      // Update the user profile to set the path of the new image. 
      $ProfilesDbLayer->set('picture', $uploadedImagesPaths['0'])
                      ->set('user_id', $_SESSION['id'])
                      ->save();
     break;    
  endswitch;
    
endif;

echo json_encode($uploadedImagesPaths);

// EOF
?>