<?php 
namespace miniboard\Views;

use miniboard\includes\Views,
    miniboard\Models\ProfilesDbLayer;




/** The Profile View to deal with the Profiles logic. 
  * @author : Thomas LHTS <thomas.lhts[at]protonmail.ch>
  * @date: 2019.07
  * @license : https://opensource.org/licenses/MIT
  * @version: 0.1a */
      
class ProfileView extends Views {


  public function onDisplay() : void {

    // Instance of the ProfilesDbLayer 
    $ProfilesDbLayer = new ProfilesDbLayer();
    $ProfilesDbLayer->inject('dbh', $this->injected['dbh']);
    
    
    // Status shorthands to template 

    $isLoggedIn = $this->injected['Users']->isLoggedIn();
    $isAdministrator = $this->injected['Config']->isAdministrator();  
    $isModerator = $this->injected['Config']->isModerator();
    $isSelfProfile = $isLoggedIn && ((isset($_GET['id']) && $_SESSION['id'] == $_GET['id']) || !isset($_GET['id'])) ? 1:0;
    
    // Status shorthands to template 
    
    $this->setVar('isLoggedIn', $isLoggedIn);
    $this->setVar('isAdministrator', $isAdministrator);
    $this->setVar('isModerator', $isModerator);
    $this->setVar('isSelfProfile', $isSelfProfile);

    // + + + + + 




    // Update profile datas 

    if(!empty($_POST)):
      if(isset($_POST['profile']['id'])):
        // Wether the user want to update its own profile or the other case, it must to be an administrator.
        if(($this->vars['isLoggedIn'] && $_POST['profile']['id'] == $_SESSION['id']) || $this->vars['isAdministrator']): 
      
          $ProfilesDbLayer->set('id', $_POST['profile']['id']);
          
          // Location has been specified 
          if(isset($_POST['profile']['location']))
            $ProfilesDbLayer->set('location', $_POST['profile']['location']);
            
          // Biograpgy has been specified 
          if(isset($_POST['profile']['biography']))
            $ProfilesDbLayer->set('biography', $_POST['profile']['biography']);
            
          // Update profile 
          $this->setVar('profileUpdatedSuccess', $ProfilesDbLayer->save());
        
        endif; 
      endif;
    endif;

    // + + + + + + + + 




    // Retrieve profile's informations

    if(isset($_GET['id'])):
      $ProfilesDbLayer->set('user_id', $_GET['id']);
      
    elseif(isset($_SESSION['id'])):
      $ProfilesDbLayer->set('user_id', $_SESSION['id']);
    
    else: 
      // No profile id to be used 
      header('Location: ' . BASE_URI);
      exit();      
    endif;
    
    $this->setVar('selectedProfile', $ProfilesDbLayer->fetch());
    
    // Hacker-mind gathering attempt or mispecified user id.
    if(empty($this->vars['selectedProfile'])):
      header('Location: ' . BASE_URI);
      exit();
    endif;
      

    // Templates 
    
    $this->setFile('header.tpl.php');
    $this->setFile('brand.tpl.php');
    $this->setFile('profiles.tpl.php');
    $this->setFile('footer.tpl.php');
  }
  
}
// EOF